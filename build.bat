set build_failed=0

cd ../bzpa1-web
call yarn build || (set build_failed=1 & set build_failed_name="bzpa1-web" )

xcopy /Y build "../bzpa1_controller/src/main/webapp" /e

cd ../bzpa1_controller

if "%build_failed%"=="0" (
cd submodule\commpacketqueuelib
call mvn -Dmaven.test.skip=true clean install || (set build_failed=1 & set build_failed_name="commpacketqueuelib" )
cd ..
)
if "%build_failed%"=="0" (
cd fx5u_dr
call mvn -Dmaven.test.skip=true clean install || (set build_failed=1 & set build_failed_name="fx5u_dr" )
cd ..
)

cd ..
)
if "%build_failed%"=="0" (
call mvn -Dmaven.test.skip=true clean install || (set build_failed=1 & set build_failed_name="current project" )
)

if "%build_failed%"=="0" (
	(
		echo [ All successed ]
	) > "target\BuildResult.txt"
	echo [INFO] ========================== [ All successed! ] ==========================
) else (
	(
		echo [ Build %build_failed_name% failed ]
	) > "target\BuildResult.txt"
	echo [INFO] ================= [ Build %build_failed_name% failed! ] ================
)
