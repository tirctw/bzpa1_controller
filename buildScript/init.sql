/* create database */
Create database bzpa1
COLLATE Chinese_Taiwan_Stroke_CI_AS
GO
USE bzpa1
GO
/* create login */
CREATE LOGIN bzpa1 WITH PASSWORD=N'bzpa1', DEFAULT_DATABASE = bzpa1, CHECK_POLICY = OFF
GO
ALTER LOGIN bzpa1 ENABLE
GO

/*========================================================================================*/

USE [bzpa1]
GO
/****** Object:  User [bzpa1]    Script Date: 2021/5/25 下午 04:10:48 ******/
CREATE USER [bzpa1] FOR LOGIN [bzpa1] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [bzpa1]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [bzpa1]
GO
ALTER ROLE [db_datareader] ADD MEMBER [bzpa1]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [bzpa1]
GO
/****** Object:  Schema [bzpa1]    Script Date: 2021/5/25 下午 04:10:48 ******/
CREATE SCHEMA [bzpa1]
GO