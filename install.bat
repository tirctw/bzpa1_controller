@echo off
set TomcatX86Path="C:\Program Files (x86)\Apache Software Foundation\Tomcat 9.0\webapps"
set TomcatPath="C:\Program Files\Apache Software Foundation\Tomcat 9.0\webapps"
set WarFile=bzpa1.war

if exist %TomcatX86Path% (copy .\target\bzpa1*.war /B %TomcatX86Path%\%WarFile%)

if exist %TomcatPath% (copy .\target\bzpa1*.war /B %TomcatPath%\%WarFile%)