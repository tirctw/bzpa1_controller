package tirc.bzpa1.bzpa1_controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableScheduling
@EnableRetry
@EnableTransactionManagement
public class Bzpa1ControllerApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(Bzpa1ControllerApplication.class, args);
	}

}
