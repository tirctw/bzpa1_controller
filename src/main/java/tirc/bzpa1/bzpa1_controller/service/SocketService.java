package tirc.bzpa1.bzpa1_controller.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import tirc.bzpa1.bzpa1_controller.model.Settings;

@Service
public class SocketService {	
	private static Logger logger = LoggerFactory.getLogger( SocketService.class ) ;
	private Settings settings = SettingService.getInstance().read();
    
    public String sendCommand(String command, int port){
//		logger.debug("command:{}", command);
        try{
        	Socket socket = new Socket(settings.getPrinterAppIp(), port);
            OutputStream output = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(output, true);
            writer.write(command);
            writer.flush();
            
            InputStream input = socket.getInputStream();
            byte[] buffer = new byte[4096];
            input.read(buffer);
            String str = new String(buffer, StandardCharsets.UTF_8);
            
            socket.close();
            output.close();
            input.close();
//            logger.debug("Response:\n\n{}", str);
   
            return str.trim();
        } catch (UnknownHostException ex) {
//        	logger.error("Socket UnknownHostException");
        } catch (IOException ex) {
//        	logger.error("Socket IOException");
        }
        return null;
    }
	
	
	public static void main(String[] args) {
		SocketService socketModel = new SocketService();
		
		logger.debug("讀取印表機狀態指令");
		printCmd(socketModel, "Getstatus");
		
		logger.debug("讀取剩餘墨水量指令");
		printCmd(socketModel, "GetCink");
		printCmd(socketModel, "GetMink");
		printCmd(socketModel, "GetYink");
		printCmd(socketModel, "GetKink");
		
		logger.debug("讀取Wiper壽命指令");
		printCmd(socketModel, "GetWiper");
		
		logger.debug("結束目前的列印JOB");
		printCmd(socketModel, "Finishprint");
		
		logger.debug("清除未列印JOB");
		printCmd(socketModel, "ClearQueue");
	}
	public static void printCmd(SocketService socketModel, String cmd) {
		logger.debug(cmd +":"+ socketModel.sendCommand(cmd, 9102));
	}

}
