package tirc.bzpa1.bzpa1_controller.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import tirc.bzpa1.bzpa1_controller.model.Bzpa1Exception;
import tirc.bzpa1.bzpa1_controller.model.Bzpa1Exception.Reason;
import tirc.bzpa1.bzpa1_controller.model.ELevel;
import tirc.bzpa1.bzpa1_controller.model.EPrinterConnected;
import tirc.bzpa1.bzpa1_controller.model.EPrinterStatus;
import tirc.bzpa1.bzpa1_controller.model.InkInfo;
import tirc.bzpa1.bzpa1_controller.model.Settings;

@Service
public class PrinterService {
	@Autowired SocketService socketService;
	@Autowired LogService logService;
	
	private static final Logger logger = LoggerFactory.getLogger(PrinterService.class);
	private EPrinterConnected printerConnected = EPrinterConnected.Offline;
	private EPrinterStatus printerStatus = EPrinterStatus.NONE;
	private Map<String, Bzpa1Exception> errors = new HashMap<String, Bzpa1Exception>();
	private Map<String, Bzpa1Exception> warns = new HashMap<String, Bzpa1Exception>();
	private InkInfo inkInfo = new InkInfo();
	private float wiper = 0.0f;
	private Settings settings = SettingService.getInstance().read();
	private int nowPrintNumber = 0;
	private boolean simulatePrintRunning;
	
	public void finishPrint() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				
				if(printerStatus == EPrinterStatus.PRINTING) {
					sleep(3000);
					getResponse("Finishprint");
				}
			}
		}).start();
	}
	
	public void reStartEng() {
		getResponse("ReStartEng");
	}
	
	public void print(String fullPath, int pdfCopys, int pageCopys) {
		String cmd =  "Printpdf," + fullPath + "," + pdfCopys + "," + pageCopys;
		getResponse(cmd);
	} 
	
	public void setToOnline() {
		getResponse("SetToOnline");
	}
	
	public void setToOffline() {
		getResponse("SetToOffline");
	}

	public void clearFail() {
		new Thread(() ->{
			getResponse("ClearFail");
		}).start();
	}


	public void endProgram() {
		new Thread(() ->{
			getResponse("EndProgram");
		}).start();
		
	}
	
	public boolean isReadyToShutDown() {
		return printerStatus==EPrinterStatus.PRIMED_IDLE || 
				printerStatus==EPrinterStatus.DEPRIMED_IDLE || 
				printerStatus==EPrinterStatus.PAUSED;
	}
	public void engineShutDown() {
		if(isReadyToShutDown()) {
			logger.debug("ShutDown start");
			getResponse("ShutDown");
			
			long last = System.currentTimeMillis();
			
			while(printerStatus != EPrinterStatus.OFF) {
				if(System.currentTimeMillis() - last > settings.getEngineShutdownTimeout())
					throw new Bzpa1Exception(Reason.ENGINE_SHUTDOWN_TIMEOUT);
				sleep(100);
			}
			logger.debug("ShutDown finished");
		}else 
			throw new Bzpa1Exception(Reason.NOT_IDLE_STATUS);
	}
	
	public EPrinterConnected getPrinterConnected() {
		return printerConnected;
	}

	public EPrinterStatus getPrinterStatus() {
		return printerStatus;
	}
	
	public boolean isExceptionHappen() {
		return !warns.isEmpty() || !errors.isEmpty();
	}
	public InkInfo getInkInfo() {
		return inkInfo;
	}

	public float getWiper() {
		return wiper;
	}

	@Scheduled(fixedDelay = 500, initialDelay = 1000)
	private void updateStatus() {	
		String response = getResponse("Getstatus");
		
		if(response == null)
			return;

		printerConnected = parsePrinterConnected(response);
		
		printerStatus = parsePrinterStatus(response);
		
		if(response.contains("Error"))
			addException(parsePrinterError(response));
		else {
			warns = new HashMap<String, Bzpa1Exception>();
			errors = new HashMap<String, Bzpa1Exception>();
		}
		
	}
	
	private String getResponse(String command) {
		String response = socketService.sendCommand(command, settings.getPrinterPort());
//		logger.debug("Cmd:{} , Response:{}", command, response);
		
		if(response == null)
			addException(new Bzpa1Exception(Reason.PRINTER_NOT_RESPONSE));
		
		return response;
	}
	
	private synchronized void addException(Bzpa1Exception e) {
		if(e.getLevel() == ELevel.WARN) {
			if(!warns.containsKey(e.getCode())) {
				warns = new HashMap<String, Bzpa1Exception>();
				errors = new HashMap<String, Bzpa1Exception>();
				
				warns.put(e.getCode(), e);
				logService.create(e.getCategory(), e.getLevel(), "["+e.getCode()+"] "+ e.getMessage());
			}
		}else {
			if(!errors.containsKey(e.getCode())) {
				warns = new HashMap<String, Bzpa1Exception>();
				errors = new HashMap<String, Bzpa1Exception>();
				
				errors.put(e.getCode(), e);
				logService.create(e.getCategory(), e.getLevel(), "["+e.getCode()+"] "+ e.getMessage());
			}
		}
	}
	
	private EPrinterConnected parsePrinterConnected(String response) {
		if(response.indexOf(EPrinterConnected.Online.name()) == 0)
			return EPrinterConnected.Online;
		else
			return EPrinterConnected.Offline;
	}
	private EPrinterStatus parsePrinterStatus(String response) {
		for(EPrinterStatus status :EPrinterStatus.values()) {
			if(response.indexOf(status.name()) > 0)
				return status;
		}
		return null;
	}
	private Bzpa1Exception parsePrinterError(String response) {
		for(Reason reason :Bzpa1Exception.Reason.values()) {
			if(response.indexOf(reason.getCode())> 0)
				return new Bzpa1Exception(reason);
		}
		return new Bzpa1Exception(Reason.PRINTER_ENGINE_ERROR);
	}
	
	@Scheduled(fixedDelay = 10000, initialDelay = 1000)
	private void updateInkInfoAndWiper() {
		inkInfo.setC(getVal(getResponse("GetCink")));
		inkInfo.setM(getVal(getResponse("GetMink")));
		inkInfo.setY(getVal(getResponse("GetYink")));
		inkInfo.setK(getVal(getResponse("GetKink")));
		
//		logger.info("ink:{}", gson.toJson(inkInfo));
		
		String str = getResponse("GetWiper");
		
		if(str==null || str.equals("Ready"))
			wiper = 100.0f;
		else if(str.equals("Low"))
			wiper = 5.0f;
		else if(str.equals("Fail") || str.equals("Out"))
			wiper = 0.0f;
	}
	private float getVal(String response) {
		if(response == null)
			return 100.0f;
		
		try {
			int idx = response.indexOf("%");
			String numStr = response.substring(0, idx);
			return Float.valueOf(numStr);			
		}catch(Exception e) {
//			logger.error("{} not convert to float.", response);
			return 0.0f;
		}
	}
	
	public Map<String, Bzpa1Exception> getErrors() {
		return errors;
	}

	public Map<String, Bzpa1Exception> getWarns() {
		return warns;
	}

	private void sleep(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getJobInfo() {
		String str = getResponse("GetJobInfo");
		
		if(str==null || str.equals("Fail") || str.equals("")) {
			nowPrintNumber = 0;
			return "";
		}
		try {
			nowPrintNumber = Integer.valueOf(str.split("/")[0]);
			return str;
		}catch(NumberFormatException e) {
			return "";
		}
	}

	public int getNowPrintNumber() {
		return nowPrintNumber;
	}
	
	public void simulatePrint(int total) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				nowPrintNumber=0;
				simulatePrintRunning = true;
				
				while(simulatePrintRunning && nowPrintNumber++ < total) {
					logger.debug("nowPrintNumber:{}, total:{}", nowPrintNumber, total);
					sleep(2000);
				}
				nowPrintNumber = 0;
			}
		}).start();
	}
	
	public void simulateStop() {
		simulatePrintRunning = false;
	}


}
