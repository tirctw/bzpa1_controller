package tirc.bzpa1.bzpa1_controller.service;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.fontbox.ttf.TrueTypeCollection;
import org.apache.fontbox.ttf.TrueTypeCollection.TrueTypeFontProcessor;
import org.apache.fontbox.ttf.TrueTypeFont;
import org.apache.pdfbox.contentstream.operator.Operator;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.pdfparser.PDFStreamParser;
import org.apache.pdfbox.pdfwriter.ContentStreamWriter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import tirc.bzpa1.bzpa1_controller.model.Bzpa1Exception;
import tirc.bzpa1.bzpa1_controller.model.EFace;
import tirc.bzpa1.bzpa1_controller.model.Bzpa1Exception.Reason;
import tirc.bzpa1.bzpa1_controller.model.EIncrease;
import tirc.bzpa1.bzpa1_controller.model.PDFInfo;
import tirc.bzpa1.bzpa1_controller.model.PrintConfiguration;
import tirc.bzpa1.bzpa1_controller.model.PrintSetting;
import tirc.bzpa1.bzpa1_controller.model.Settings;
import tirc.bzpa1.bzpa1_controller.repository.BoxVariable;
import tirc.bzpa1.bzpa1_controller.repository.ProfileSize;
import tirc.bzpa1.bzpa1_controller.repository.ProfileVariable;

@Service
public class CreatePDFService {
	private static final Logger logger = LoggerFactory.getLogger(CreatePDFService.class);
	private PDDocument document;
	private final String BEGIN_STR = "$";
	private final String END_STR = "}";
	/*
	1 pt = 1/72 inch
	mm = pt*25.4 / 72
	*/
	private final float MM_PER_PT = 25.4f/72.0f;
	
	@Autowired BoxVariableService boxVariableService;
	@Autowired ScalePDFService scaleService;
	Settings settings = SettingService.getInstance().read();
	private PDFont font = PDType1Font.HELVETICA_BOLD;
	private boolean bFontLoad = false;
	
	enum Std14FontEnum {
		TIMES_ROMAN(PDType1Font.TIMES_ROMAN),
		TIMES_BOLD(PDType1Font.TIMES_BOLD),
		TIMES_ITALIC(PDType1Font.TIMES_ITALIC),
		TIMES_BOLD_ITALIC(PDType1Font.TIMES_BOLD_ITALIC),
		HELVETICA(PDType1Font.HELVETICA),
		HELVETICA_BOLD(PDType1Font.HELVETICA_BOLD),
		HELVETICA_OBLIQUE(PDType1Font.HELVETICA_OBLIQUE),
		HELVETICA_BOLD_OBLIQUE(PDType1Font.HELVETICA_BOLD_OBLIQUE),
		COURIER(PDType1Font.COURIER),
		COURIER_BOLD(PDType1Font.COURIER_BOLD),
		COURIER_OBLIQUE(PDType1Font.COURIER_OBLIQUE),
		COURIER_BOLD_OBLIQUE(PDType1Font.COURIER_BOLD_OBLIQUE),
		SYMBOL(PDType1Font.SYMBOL),
		ZAPF_DINGBATS(PDType1Font.ZAPF_DINGBATS);
		
		private PDFont font;
		Std14FontEnum(PDType1Font font) {
			this.font = font;
		}
		public PDFont getFont() {
			return this.font;
		}
		
		public static Std14FontEnum valueOfIgnoreCase(String name) {
			
			for (Std14FontEnum stdFont: values()) {
				if (stdFont.name().equalsIgnoreCase(name)) {
					return stdFont;
				}
			}
		
			return null;
		}
		
	}
	
	public void loadFont() throws IOException {
		
		// RESET FONT INFO
		font = PDType1Font.HELVETICA_BOLD;
		bFontLoad = false;
		
		String fontName = settings.getFontName();
		String fontPath = settings.getFontPath();
		
		// TRY STD 14 FONTS
		Std14FontEnum stdFont = Std14FontEnum.valueOfIgnoreCase(fontName);
		if (null != stdFont) {
			font = stdFont.getFont();
			logger.info("use std 14 font:{}", stdFont.name());
			return;
		}
				
		// TRY LOAD TTF FILE 
		File fontFile = new File(fontPath,fontName);
		String extName = FilenameUtils.getExtension(fontFile.getPath());
		
		if(fontFile.exists()){
			if(extName.equalsIgnoreCase("ttc")) {
				TrueTypeCollection ttc = new TrueTypeCollection(fontFile);
				
				ttc.processAllFonts(new TrueTypeFontProcessor () {
					@Override
					public void process(TrueTypeFont ttf) throws IOException {
						if (!bFontLoad) {
							logger.info("loaded ttf:{}", ttf.getName());
							font = PDType0Font.load(document, ttc.getFontByName(ttf.getName()), true);
							bFontLoad = true;
						}
					}
				});

			}
			else if(extName.equalsIgnoreCase("ttf")) {
				font = PDType0Font.load(document, new FileInputStream(fontFile), true);
				bFontLoad = true;
			}
		} else {
			logger.error("Load font fail. {}", fontFile.getAbsolutePath());
		}
		
		logger.info("use font: {}", font.getName());
		
	}
	public class PositionInfo {
		int page;
		TextPosition pos;
		
		public PositionInfo(int page, TextPosition pos) {
			this.page = page;
			this.pos = pos;
		}
		
		public int getPage() {
			return page;
		}
		
		public TextPosition getPos() {
			return pos;
		}
	}
	
	public PDFInfo findInfo(String fileName) throws IOException {		
		document = PDDocument.load(new File(fileName));
		
		//find size
		PDRectangle rectangle = document.getPage(0).getMediaBox();
		ProfileSize size = new ProfileSize((int)(rectangle.getWidth()*MM_PER_PT), (int)(rectangle.getHeight()*MM_PER_PT), EFace.ALL);

		//find variable
		List<BoxVariable> foundVariables = findVariables(boxVariableService.findAll());
		
		PDFInfo info = new PDFInfo();
		info.setSize(size);
		info.setVariables(foundVariables);
		
		document.close();
		
		return info;
	}
	
	private List<BoxVariable> findVariables(List<BoxVariable> variables) throws IOException {
		List<BoxVariable> foundVariables = new ArrayList<BoxVariable>();
		
		Map<String, List<PositionInfo>> map = searchStringPosByBoxVariable(variables);
		
		for(BoxVariable variable : variables) {
			if(map.containsKey(variable.getKey()))
				foundVariables.add(variable);
		}
		
    	return foundVariables;
	}
	
    private Map<String, List<PositionInfo>> searchStringPosByProfileVariable(List<ProfileVariable> variables) throws IOException
    {
    	Map<String, List<PositionInfo>> posMap = new HashMap<String, List<PositionInfo>>();
    	
    	for(ProfileVariable variable : variables) {
    		String searchTerm = variable.getKey();
//            System.out.printf("* Looking for '%s'\n", searchTerm);
            
            List<PositionInfo> list = new ArrayList<PositionInfo>();
            
            //only search first page
            for(int i=1; i<=1  /*document.getNumberOfPages()*/; i++) {
            	findSubwords(i, searchTerm, list);
            }
            
            if(!list.isEmpty())
            	posMap.put(searchTerm, list);
    	}
        return posMap;
    }
    
    private Map<String, List<PositionInfo>> searchStringPosByBoxVariable(List<BoxVariable> variables) throws IOException
    {
    	Map<String, List<PositionInfo>> posMap = new HashMap<String, List<PositionInfo>>();
    	
    	for(BoxVariable variable : variables) {
    		String searchTerm = variable.getKey();
//            System.out.printf("* Looking for '%s'\n", searchTerm);
            
            List<PositionInfo> list = new ArrayList<PositionInfo>();
            
            //only search first page
            for(int i=1; i<=1  /*document.getNumberOfPages()*/; i++) {
            	findSubwords(i, searchTerm, list);
            }
            
            if(!list.isEmpty())
            	posMap.put(searchTerm, list);
    	}
        return posMap;
    }
    
    
    private void findSubwords(int page, String searchTerm, List<PositionInfo> list) throws IOException
    {
        PDFTextStripper stripper = new PDFTextStripper()
        {
            @Override
            protected void writeString(String text, List<TextPosition> textPositions) throws IOException
            {
            	int idx = text.toUpperCase().indexOf(searchTerm.toUpperCase());
                if(idx != -1) {
                	list.add(new PositionInfo(page, textPositions.get(idx)));
                }
                super.writeString(text, textPositions);
            }
        };
        stripper.setSortByPosition(true);
        stripper.setStartPage(page);
        stripper.setEndPage(page);
        stripper.getText(document);
    }
    
	public String generatePrintPDF(PrintConfiguration configuration, String targetFileName) throws Exception {
		logger.info("File Path:{}", configuration.getFilePath());
		File file = new File(configuration.getFilePath());
		
		if(!file.exists())
			throw new FileNotFoundException();
	
		File tmpFile = new File(settings.getPDFPath() + "tmp.pdf");

		if(tmpFile.exists())
			tmpFile.delete();
		
		PDDocument tmpDoc = new PDDocument();
		tmpDoc.save(tmpFile);
		tmpDoc.close();	
		
		Map<String, Integer> vals = new HashMap<String, Integer>();
		
		//find all auto increase variable
		for(ProfileVariable val : configuration.getVariables()) {
			if(val.getIncOp() != EIncrease.NONE && isNumeric(val.getValue())) {
				vals.put(val.getKey(), Integer.valueOf(val.getValue()));
			}
		}
		//copy find
		int copies = configuration.getSettings().getCopies();
		
		for(int i=0;i<copies;i++) {
			for(ProfileVariable v : configuration.getVariables()) {
				if(vals.containsKey(v.getKey())) {
					int targetLen = Integer.valueOf(v.getFormat());
					int operator = v.getIncOp() == EIncrease.INCREASE ? 1 : -1 ;
					int num = vals.get(v.getKey()) + i*operator;
					
					if(num < 1) num = 1;
					
					int nowLen = String.valueOf(num).length();
					StringBuilder sb = new StringBuilder();
					
					for(int j=0;j<targetLen-nowLen;j++)
						sb.append("0");
					
					sb.append(String.valueOf(num));
					
					v.setValue(sb.toString());
				}
			}
			PDDocument newDoc = PDDocument.load(tmpFile);
			
			document = PDDocument.load(file);
			
			loadFont();
			
			Map<String, List<PositionInfo>> posMap = searchStringPosByProfileVariable(configuration.getVariables());

			removeSpecificStr(posMap.keySet());
			
			addText(configuration.getVariables(), posMap);
		
			newDoc.addPage(document.getPage(0));
			
			// embedded font with minimal set
			if (this.bFontLoad) this.font.subset();
			
			newDoc.save(tmpFile);
			  
		    //Closing the document
		    document.close();
		    newDoc.close();
		}
		PrintSetting printSettings = configuration.getSettings();
		
		scaleService.resize(tmpFile.getAbsolutePath(),
				printSettings.getSize().getWidth(),
				printSettings.getSize().getHeight(),
				printSettings.getOffset(), targetFileName);
		
		String targetPath = settings.getPDFPath() + targetFileName ;	
		
		tmpFile.delete();	
		
		if(configuration.isGrayScale()) {
			logger.info("convert the pdf to gray scale");
			FileUtils.moveFile(new File(targetPath), tmpFile);	    
			convertGrayscale(tmpFile.getCanonicalPath(), targetPath);
			tmpFile.delete();	
		}
	    		
		return targetPath;
	}
    private boolean isNumeric(String str){
    	for(int i=0;i<str.length();i++) {
    		if(!Character.isDigit(str.charAt(i)))
    			return false;
    	}
    	return true;
    }
    
    private void removeSpecificStr(Set<String> strSet) throws IOException {
    	String totalStr = getAllStr().toUpperCase();
    	
    	List<Integer> removeIdxs = new ArrayList<Integer>();
    	
    	addRemovedIdx(strSet, totalStr, removeIdxs);

    	removeText(removeIdxs);
    }
    
    private String getAllStr() throws IOException {
        PDPageTree pages = document.getDocumentCatalog().getPages();
        StringBuilder sb = new StringBuilder();
        
        for (PDPage page : pages) {
            PDFStreamParser parser = new PDFStreamParser(page);
            parser.parse();
            List tokens = parser.getTokens();
            
            for (int j = 0; j < tokens.size(); j++) {
                Object next = tokens.get(j);
                if (next instanceof Operator) {
                    Operator op = (Operator) next;
                    // Tj and TJ are the two operators that display strings in a PDF
                    // Tj takes one operator and that is the string to display so lets update that operator
                    
                    if (op.getName().equals("Tj")) {
                        COSString previous = (COSString) tokens.get(j-1);
                        String string = previous.getString();
                        sb.append(string);
                        
                    } else if (op.getName().equals("TJ")) {
                        COSArray previous = (COSArray) tokens.get(j-1);
                        for (int k = 0; k < previous.size(); k++) {
                            Object arrElement = previous.getObject(k);
                            if (arrElement instanceof COSString) {
                                COSString cosString = (COSString) arrElement;
                                String string = cosString.getString();
                                sb.append(string);
                            }
                        }
                    }
                }
            }
        }
        
        return sb.toString();
    }
    
    private void addRemovedIdx(Set<String> strSet, String totalStr, List<Integer> removeIdxs) {
    	for(String str : strSet) {
    		str = str.toUpperCase();
    		int idx = totalStr.indexOf(str);
    		
    		while(idx != -1) {
    			
    			for(int i=idx;i<idx+str.length();i++) {
    				removeIdxs.add(i);
    			}
    			idx = str.length() + idx;
    			idx = totalStr.indexOf(str, idx);
    		}
    	}
    }
    
    private void removeText(List<Integer> removeIdxs) throws IOException {
        PDPageTree pages = document.getDocumentCatalog().getPages();
        int idx = 0;
        
        for (PDPage page : pages) {
            PDFStreamParser parser = new PDFStreamParser(page);
            parser.parse();
            List tokens = parser.getTokens();
            
            for (int j = 0; j < tokens.size(); j++) {
                Object next = tokens.get(j);
                if (next instanceof Operator) {
                    Operator op = (Operator) next;
                    // Tj and TJ are the two operators that display strings in a PDF
                    // Tj takes one operator and that is the string to display so lets update that operator
                    
                    if (op.getName().equals("Tj")) {
                        COSString previous = (COSString) tokens.get(j-1);
                        String string = previous.getString();
                        
//                        System.out.println("str1:"+string);
                        StringBuilder sb = new StringBuilder();
                        
                        for(int i=0;i<string.length();i++) {
                        	if(removeIdxs.contains(idx++))
                        		sb.append("");  //use empty to replace space, the space char maybe not embedded. 
                        	else
                        		sb.append(string.charAt(i));
                        }
                        COSString cosstring = new COSString(sb.toString());
                        previous.setValue(cosstring.getBytes());
                        
                    } else if (op.getName().equals("TJ")) {
                        COSArray previous = (COSArray) tokens.get(j-1);
                        for (int k = 0; k < previous.size(); k++) {
                            Object arrElement = previous.getObject(k);
                            if (arrElement instanceof COSString) {
                                COSString cosString = (COSString) arrElement;
                                String string = cosString.getString();
                                
//                                System.out.println("str2:"+string);
                                StringBuilder sb = new StringBuilder();
                                
                                for(int i=0;i<string.length();i++) {
                                	if(removeIdxs.contains(idx++))
                                		sb.append("");  //use empty to replace space, the space char maybe not embedded.
                                	else
                                		sb.append(string.charAt(i));
                                }
                                
                                cosString.setValue(new COSString(sb.toString()).getBytes());
                            }
                        }
                    }
                }
            }
            // now that the tokens are updated we will replace the page content stream.
            PDStream updatedStream = new PDStream(document);
            OutputStream out = updatedStream.createOutputStream();
            ContentStreamWriter tokenWriter = new ContentStreamWriter(out);
            tokenWriter.writeTokens(tokens);
            out.close();
            page.setContents(updatedStream);
        }
    }
    private void addText(List<ProfileVariable> variables, Map<String, List<PositionInfo>> posMap) throws IOException{
    	for(ProfileVariable variable : variables) {
    		String origin = variable.getKey();
    		String replace = variable.getValue();
    		
    		List<PositionInfo> list = posMap.get(origin);
    		
    		if(list != null) {
    			for(PositionInfo info:list) {
    				addText(info.getPage()-1, replace, info.getPos());    				
    			}
    		}
    	}
    }
	private void addText(int pageIdx, String text, TextPosition pos) throws IOException {	       
	      //Retrieving the pages of the document
	      PDPage page = document.getPage(pageIdx);
	      
	      //Begin the Content stream
	      PDPageContentStream contentStream = new PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND,true,false);
	      contentStream.setNonStrokingColor(loadColor());

	      contentStream.beginText(); 
	      
	      //Setting the font to the Content stream
	      contentStream.setFont(font, pos.getFontSize());
    	  
//    	  logger.info("text:{} endX:{}, endY:{}, height:{}, width:{}, X:{}, Y:{}, xScale:{}, yScale:{}", text,
//    			  pos.getEndX(), pos.getEndY(),
//    			  pos.getHeight(), pos.getWidth(),
//    			  pos.getX(), pos.getY(),
//    			  pos.getXScale(), pos.getYScale());

//    	  contentStream.newLineAtOffset(pos.getEndX() - pos.getWidth(), pos.getEndY() );
	      contentStream.setTextMatrix(pos.getTextMatrix());
    	  
	      try {
	    	  contentStream.showText(text);	    	  
	      }catch(IllegalArgumentException e) {
		      //Ending the content stream
		      contentStream.endText();
		      //Closing the content stream
		      contentStream.close();
		      
	    	  throw new Bzpa1Exception(Reason.FONT_NOT_SUPPORT);
	      }
	      //Ending the content stream
	      contentStream.endText();
	      //Closing the content stream
	      contentStream.close();
	}
	private Color loadColor() {
		Color color;
		try {
		    Field field = Class.forName("java.awt.Color").getField(settings.getFontColor());
		    color = (Color)field.get(null);
		} catch (Exception e) {
		    color = Color.BLACK;
		}
		return color;
	}

	private void save(String fileName) throws IOException {
//	      //Saving the document
		  PDDocument newDoc = new PDDocument();
		  newDoc.addPage(document.getPage(0));
		  newDoc.save(new File(fileName));
		  
	      //Closing the document
	      document.close();
	      newDoc.close();

	     	      
	}
	
	public static void convertGrayscale(String inPdfPath, String outPdfPath) throws IOException, DocumentException {
		PdfReader pdfReader = new PdfReader(inPdfPath);
		OutputStream outputStream = new FileOutputStream(new File(outPdfPath));
		PdfStamper pdfStamper = new PdfStamper(pdfReader, outputStream);
		dropSaturation(pdfStamper);
		pdfStamper.close();
		pdfReader.close();
	}
	
	private static void dropSaturation(PdfStamper pdfStamper) {
	    PdfGState gstate = new PdfGState();
	    gstate.setBlendMode(PdfName.SATURATION);
	    PdfReader pdfReader = pdfStamper.getReader();
	    for (int i = 1; i <= pdfReader.getNumberOfPages(); i++) {
	        PdfContentByte canvas = pdfStamper.getOverContent(i);
	        canvas.setGState(gstate);
	        Rectangle mediaBox = pdfReader.getPageSize(i);
	        canvas.setColorFill(BaseColor.BLACK);
	        canvas.rectangle(mediaBox.getLeft(), mediaBox.getBottom(), mediaBox.getWidth(), mediaBox.getHeight());
	        canvas.fill();
	        canvas = pdfStamper.getUnderContent(i);
	        canvas.setColorFill(BaseColor.WHITE);
	        canvas.rectangle(mediaBox.getLeft(), mediaBox.getBottom(), mediaBox.getWidth(), mediaBox.getHeight());
	        canvas.fill();
	    }
	}
	
	
}
