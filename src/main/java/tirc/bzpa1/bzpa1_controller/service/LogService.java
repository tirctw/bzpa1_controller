package tirc.bzpa1.bzpa1_controller.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import tirc.bzpa1.bzpa1_controller.model.ECategory;
import tirc.bzpa1.bzpa1_controller.model.ELevel;
import tirc.bzpa1.bzpa1_controller.repository.Log;
import tirc.bzpa1.bzpa1_controller.repository.LogRepository;

@Service
public class LogService {
	private static final Logger logger = LoggerFactory.getLogger(LogService.class);
	
	@Resource private LogRepository logRepository; 
	@Resource private JdbcTemplate jdbcTemplate ;
	Gson gson = new Gson();
	
	@Transactional
	public void create(ECategory category, ELevel level, String message) {
		String pattern = "yyyy-MM-dd HH:mm:ss";

		// Create an instance of SimpleDateFormat used for formatting 
		// the string representation of date according to the chosen pattern
		DateFormat df = new SimpleDateFormat(pattern);

		// Get the today date using Calendar object.
		Date today = Calendar.getInstance().getTime();        
		// Using DateFormat format method we can create a string 
		// representation of a date with the defined format.
		String todayAsString = df.format(today);
		
		Log log = new Log(category, level, message, todayAsString);
		logger.debug(gson.toJson(log));
		logRepository.save(log);
	}

	public List<Log> findByFilter(String level, String category,String fromDate, String toDate) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.setTime(sdf.parse(toDate));
		c.add(Calendar.DATE, 1);  // number of days to add
		toDate = sdf.format(c.getTime());  // dt is now the new date
		StringBuilder sb = new StringBuilder();
		sb.append("select * from log where (created_ts between \'" + fromDate + "\' and \'" + toDate + "\') " );
		
		if(!level.equals("null"))
			sb.append(" and level = " + ELevel.valueOf(level).getSN());
		if(!category.equals("null"))
			sb.append(" and category = " + ECategory.valueOf(category).getSN());
		sb.append(" order by created_ts desc");
		logger.info(sb.toString());
		return jdbcTemplate.query(sb.toString(), new BeanPropertyRowMapper<Log>(Log.class));
	}
	
}
