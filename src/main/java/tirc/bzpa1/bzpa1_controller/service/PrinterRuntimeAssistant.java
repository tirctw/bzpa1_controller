package tirc.bzpa1.bzpa1_controller.service;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import tirc.bzpa1.bzpa1_controller.model.Settings;

@Service
public class PrinterRuntimeAssistant {
	private Settings settings = SettingService.getInstance().read();
	
	private final String TASKLIST = "tasklist";
	private final String KILL = "taskkill /F /IM ";

	
	public String listTasks(){
		try {
			Process p = Runtime.getRuntime().exec(TASKLIST);		 
			String str = IOUtils.toString(p.getInputStream(), StandardCharsets.UTF_8.name());		 		 
			return str;
		} catch (Exception ex) {
			return "";
		}
	}

	public boolean killProcess(String serviceName)  {
		try {
			Runtime.getRuntime().exec(KILL + serviceName);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	public boolean ping(String ipAddress)  {
	       int  timeOut =  3000 ;  
	       boolean status;
			try {
				status = InetAddress.getByName(ipAddress).isReachable(timeOut);
				return status;
			} catch (Exception e) {
				return false;
			}  
	}

	public boolean runProcess(String path)  {

		ProcessBuilder pb = new ProcessBuilder(path);
		pb.directory(new File(FilenameUtils.getFullPath(path)));
		try {
			pb.start();
			return true;
		} catch (IOException e) {
			return false;
		}
	}
	
	public boolean killMainWin() {
		return killProcess(settings.getMainWinAppName());
	}
	
	public boolean executeMainWin() {
		return runProcess(settings.getMainWinAppPath());
	}
	
	public boolean killTftpd() {
		return killProcess(settings.getTftpdAppName());
	}
	
	public boolean executeTftpd() {
		return runProcess(settings.getTftpdAppPath());
	}
	
	public boolean isTftpdRunning() {
		return listTasks().contains(settings.getTftpdAppName());
	}
	
	public boolean isMainWinRunning() {
		return listTasks().contains(settings.getMainWinAppName());
	}
	
	public boolean isPrinterPingable() {
		return ping(settings.getPrinterDeviceIp());
	}
	
}