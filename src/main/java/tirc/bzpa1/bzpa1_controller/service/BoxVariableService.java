package tirc.bzpa1.bzpa1_controller.service;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tirc.bzpa1.bzpa1_controller.repository.BoxVariable;
import tirc.bzpa1.bzpa1_controller.repository.BoxVariableRepository;

@Service
public class BoxVariableService {
	@Autowired BoxVariableRepository dao;
	
	@Transactional
	public BoxVariable update(BoxVariable val) {
		BoxVariable foundVal = dao.findByKey(val.getKey());
		
		if(foundVal != null)
			val.setId(foundVal.getId());
		
		return dao.save(val);
	} 
	
	public List<BoxVariable> findAll(){
		List<BoxVariable> list = new ArrayList<BoxVariable>();
		
		for(BoxVariable s : dao.findAll()) {
			list.add(s);
		}
		
		return list;
	}
	
	@Transactional
	public BoxVariable deleteByKey(String key) throws FileNotFoundException {
		BoxVariable foundVal = dao.findByKey(key);
		
		if(foundVal == null)
			throw new FileNotFoundException();
		
		dao.deleteById(foundVal.getId());
		return foundVal;
	}
}
