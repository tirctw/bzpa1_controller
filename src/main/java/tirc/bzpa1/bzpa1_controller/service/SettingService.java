package tirc.bzpa1.bzpa1_controller.service;

import java.io.File;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import tirc.bzpa1.bzpa1_controller.model.Settings;


public class SettingService {
	private static final Logger logger = LoggerFactory.getLogger(SettingService.class);
	public final static String FILE_NAME = "settings.json";
	public final static String FILE_PATH = System.getenv("PUBLIC") + File.separator + "BZPA1";
	public final static String FILE_NAME_WITH_PATH = FILE_PATH + File.separator + FILE_NAME;
	private Gson gson = new GsonBuilder().setPrettyPrinting().create();
	private static SettingService settingService; 
	private static Settings settings;
	
	public static void main(String[] args) {
		SettingService s = new SettingService();
		Settings settings = s.read();
		logger.info("data:{}", settings.getPDFPath());
	}
	
	public static SettingService getInstance() {
		if(settingService == null) {
			settingService = new SettingService();
		}
		return settingService;
	}
	
	public void save(Settings settings) {
		   try {
			   	String s = gson.toJson(settings);
		    	FileUtils.write(new File(FILE_NAME_WITH_PATH), s, StandardCharsets.UTF_8);
		   } catch (Exception ex) {
		    logger.warn("failed to write "+FILE_NAME_WITH_PATH, ex);      
		   }
	 }
	
	public Settings read() {
		if(settings == null) {
			try {	    	
		    	String readStr = FileUtils.readFileToString(new File(FILE_NAME_WITH_PATH), StandardCharsets.UTF_8);
		    	settings = gson.fromJson(readStr, Settings.class);
		    	return settings;
			} catch (Exception ex) {
				logger.warn("failed to read "+FILE_NAME_WITH_PATH, ex);
				settings = new Settings();
				save(settings);
				return settings; 
			}
		}else
			return settings;
	}
}
