package tirc.bzpa1.bzpa1_controller.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import com.fx5u.cmd.CmdSocketErrorType;
import com.fx5u.plc.app.IPCCmdAppInf;
import com.fx5u.plc.app.IPCCmdStatusType;
import com.fx5u.plc.app.PlcCmdDispatcher;

import tirc.bzpa1.bzpa1_controller.model.Bzpa1Exception;
import tirc.bzpa1.bzpa1_controller.model.ECategory;
import tirc.bzpa1.bzpa1_controller.model.Bzpa1Exception.Reason;
import tirc.bzpa1.bzpa1_controller.model.EConveyorStatus;
import tirc.bzpa1.bzpa1_controller.model.ELevel;
import tirc.bzpa1.bzpa1_controller.model.ELight;
import tirc.bzpa1.bzpa1_controller.model.PlatformStatus;
import tirc.bzpa1.bzpa1_controller.model.PlcEventHandler;
import tirc.bzpa1.bzpa1_controller.model.Settings;

@Service
public class PlcService implements IPCCmdAppInf{
	private static Logger logger = LoggerFactory.getLogger( PlcService.class ) ;
	@Autowired LogService logService;
	private Settings settings = SettingService.getInstance().read();
	private ELight light = ELight.NONE;
	private EConveyorStatus conveyorStatus = EConveyorStatus.STOP;
	private PlcCmdDispatcher plcCmdDispatcher; 
	private int cmdSerialId = 0;
	private int sensorQueryData = 0;
	private boolean bConveyorRunning = false;
	private boolean bEmergency = false;
	private boolean bShortageSignal = false;
	private boolean bFull = false;
	private boolean bStuck = false;
	private boolean bPlatformError = false;
	private Map<String, Bzpa1Exception> errors = new HashMap<String, Bzpa1Exception>();
	private Map<String, Bzpa1Exception> warns = new HashMap<String, Bzpa1Exception>();
	private PlcEventHandler plcEventHandler;
	private boolean bBoxIncomingSignal = false;
	private boolean bBoxIncomingEvent = false;
	private boolean bBoxLeavingSignal = false;
	private boolean bGrabLoop = false;
	private boolean bPrintProcedureLoop = false;
	private boolean bDischarge = false;
	private int printTotalPage = 0;
	private int printNowPage = 0;
	private int CONTROL_MODE = 16;
	
	public void setEventHandler(PlcEventHandler plcEventHandler) {
		this.plcEventHandler = plcEventHandler;
	}
	
	public boolean isExceptionHappen() {
		return !warns.isEmpty() || !errors.isEmpty();
	}
	
	public Map<String, Bzpa1Exception> getErrors() {
		return errors;
	}

	public Map<String, Bzpa1Exception> getWarns() {
		return warns;
	}

	@Retryable(include = {Bzpa1Exception.class},
            maxAttempts = 3,
            backoff = @Backoff(value = 2000))	
	public void init() {
		plcCmdDispatcher = new PlcCmdDispatcher(settings.getPlcId(), settings.getPlcIp(), settings.getPlcPort());
		plcCmdDispatcher.setAppHandler(this);

		if(plcCmdDispatcher.open()) {
			logger.info("PlcCmdDispatcher open successfully.");
			
			if(plcCmdDispatcher.init()) 
				logger.error("PlcCmdDispatcher init successfully.");
			else 
				throw new Bzpa1Exception(Reason.PLC_INIT_FAILED);
		}
		else
			throw new Bzpa1Exception(Reason.PLC_OPEN_FAILED);
	}
	
    @Recover
    public void recover(Bzpa1Exception e) {
        logger.info("PLC init failed.");
        addException(e);
    }
    
	private synchronized void addException(Bzpa1Exception e) {
		if(e.getLevel() == ELevel.WARN) {
			if(!warns.containsKey(e.getCode())) {
				warns.put(e.getCode(), e);
				logService.create(e.getCategory(), e.getLevel(), "["+e.getCode()+"] "+ e.getMessage());
			}
		}else {
			if(!errors.containsKey(e.getCode())) {				
				errors.put(e.getCode(), e);
				logService.create(e.getCategory(), e.getLevel(), "["+e.getCode()+"] "+ e.getMessage());
			}
		}
	}
	
	private void removeException(Bzpa1Exception e) {
		if(e.getLevel() == ELevel.WARN) {
			if(warns.containsKey(e.getCode())) {
				warns.remove(e.getCode());
			}
		}else {
			if(errors.containsKey(e.getCode())) {
				errors.remove(e.getCode());
			}
		}
	}
/*
控制指令輸入D1000 32bit
D1000.0 控制亮三色燈 紅					1
D1000.1 控制亮三色燈 黃					2
D1000.2 控制亮三色燈 綠					4
D1000.3 控制馬達on/off		            8
D1000.4 控制模式				            16

控制指令輸入D1001 32bit
D1001.0 控制進料							1

 */
	public void setIndicatorLight(ELight light) {
		if(this.light != light) {
			this.light = light;
			logger.debug("set light :{}", light);
			
			setPlcCmd(settings.getPlcInputCmdIdxForConveyor(), light.getSN() + conveyorStatus.getSN() + CONTROL_MODE);
		}
	}
	
	public ELight findIndicatorLight() {
		return light;
	}
	
	public void setConveyor(EConveyorStatus status) {
		if(conveyorStatus != status) {
			conveyorStatus = status;
			logger.debug("set conveyor {}", status);
			
			setPlcCmd(settings.getPlcInputCmdIdxForConveyor(), light.getSN() + conveyorStatus.getSN() + CONTROL_MODE);
		}
	}
	
	public void resetPrintPage(int totalPage) {
		printTotalPage = totalPage;
		printNowPage = 0;
	}
	
	public int getPrintTotalPage() {
		return printTotalPage;
	} 
	public int getPrintNowPage() {
		return printNowPage;
	}
	public void startPrintProcedure(int num) {
		if(!bPrintProcedureLoop) {
			bPrintProcedureLoop = true;
			
			setConveyor(EConveyorStatus.RUN);
			
			while(bPrintProcedureLoop && printNowPage < printTotalPage) {
				
				bBoxIncomingEvent = false;
				
				grabBox();
				
				long last = System.currentTimeMillis();
				
				while(!bBoxIncomingEvent && bPrintProcedureLoop) {
					if(System.currentTimeMillis()-last > settings.getGrabBoxTimeout()) {
						addException(new Bzpa1Exception(Reason.GRAB_TIMEOUT));
						bPrintProcedureLoop = false;
					}
					sleep(100);
				}
			}
			//等待最後一個箱子進入出料區	
			sleep(5000); 
			
			setConveyor(EConveyorStatus.STOP);
			
			plcEventHandler.printFinished();
			
			bPrintProcedureLoop = false;
		}
	}
	
	public void grabBoxRepeatedly() {
		sleep(1000);
		
		if(!bGrabLoop) {
			new Thread(() -> {
				bGrabLoop = true;
				
				while(bGrabLoop) {
					grabAction();
					sleep(2500);
				}
			}).start();
		}
	}
	
	public void stopPrintProcedure() {
		bPrintProcedureLoop = false;
	}
	
	public void stopGrab() {
		bGrabLoop = false;
	}
	
	public void grabBox() {
		if(bShortageSignal) {
			logger.debug("box shortage");
			plcEventHandler.stopPrint();
//			addException(new Bzpa1Exception(Reason.BOX_SHORTAGE));
		}else if(bStuck) {
			logger.debug("box stuck");
			addException(new Bzpa1Exception(Reason.BOX_STUCK));
		}else {
			grabAction();
			
			printNowPage++;
		}
	} 
	
	public void grabAction() {
		logger.debug("grab box");
		setPlcCmd(settings.getPlcInputCmdIdxForGrabber(), 1);
	}
	
	private void setPlcCmd(int cmdIdx, int value) {
		logger.debug("cmdIdx:{} , value:{}  ", cmdIdx, value);
		if(plcCmdDispatcher != null) {
			plcCmdDispatcher.sendDataSetCmd(cmdSerialId++, cmdIdx, value);
		}
	}
	
	public PlatformStatus getPlatformStatus() {
		return new PlatformStatus(bBoxIncomingSignal, bBoxLeavingSignal, bConveyorRunning, bPlatformError, bEmergency,
				bShortageSignal, bFull, bStuck, bDischarge, bGrabLoop, bPrintProcedureLoop);
	}	
	@Override
	public synchronized void cbSensorQueryData(byte[] data, int size) {
		int value = (data[0]&0xFF) + ((0xFF&data[1])<<8) + ((0xFF&data[2])<<16) + ((0xFF&data[3])<<24);
		if(sensorQueryData != value){
			logger.debug("cbSensorQueryData=[{},{}]", value, size);
			sensorQueryData = value;
			setValue(sensorQueryData);
		}
	}
	/*
控制指令輸出D1010 32bit
D1010.0	異常狀態		1(馬達異常)
D1010.1	入料中		2(入料)
D1010.2	出料中		4(箱體碰觸到下降sensor)
D1010.3	軌道運轉中	8
D1010.4	緊急停止		16
D1010.5	缺料			32
D1010.6	滿料			64
D1010.7	卡料			128
D1010.8	出料台狀態   256(卸貨)

	 */
	private void setValue(int data) {
		if((data & 1) == 1) {
			bPlatformError = true;
			addException(new Bzpa1Exception(Reason.CONVEYOR_BROKEN));
		}else {
			bPlatformError = false;
			removeException(new Bzpa1Exception(Reason.CONVEYOR_BROKEN));
		}
		
		if((data & 2) == 2) {
			bBoxIncomingSignal = true;
		}else{
			if(bBoxIncomingSignal) 
				bBoxIncomingEvent = true;
			
			bBoxIncomingSignal = false;
		}
			
		if((data & 4) == 4)
			bBoxLeavingSignal = true;
		else
			bBoxLeavingSignal = false;
		
		
		if((data & 8) == 8)
			bConveyorRunning = true;
		else
			bConveyorRunning = false;
		
		if((data & 16) == 16) {
			bEmergency = true;
			addException(new Bzpa1Exception(Reason.PLC_EMERGENCY_STOP));
		}
		else {
			bEmergency = false;
			removeException(new Bzpa1Exception(Reason.PLC_EMERGENCY_STOP));
		}
		
		if((data &32) == 32)
			bShortageSignal = true;
		else {
			bShortageSignal = false;
//			removeException(new Bzpa1Exception(Reason.BOX_SHORTAGE));
		}
		
		if((data &64) == 64) {
			bFull = true;
			addException(new Bzpa1Exception(Reason.DISCHARGE_FULL));
		}
		else {
			bFull = false;
			removeException(new Bzpa1Exception(Reason.DISCHARGE_FULL));
		}
		
		if((data &128) == 128)
			bStuck = true;
		else {
			bStuck = false;
			removeException(new Bzpa1Exception(Reason.BOX_STUCK));
		}
		
		if((data &256) == 256)
			bDischarge = true;
		else 
			bDischarge = false;
		
	}

	@Override
	public synchronized void cbErrorEvent(CmdSocketErrorType event) {
		Bzpa1Exception e = null;
		
		if(event == CmdSocketErrorType.SOCKET_ABNORMAL_FORMAT)
			e = new Bzpa1Exception(Reason.PLC_ABNORMAL_FORMAT);
		else if(event == CmdSocketErrorType.SOCKET_IO_ERROR)
			e = new Bzpa1Exception(Reason.PLC_IO_ERROR);
		else if(event == CmdSocketErrorType.SOCKET_LOGIC_ERROR)
			e = new Bzpa1Exception(Reason.PLC_LOGIC_ERROR); 
		else if(event == CmdSocketErrorType.SOCKET_CMD_TIMEOUT)
			e = new Bzpa1Exception(Reason.PLC_CMD_TIMEOUT);
		
		if(e != null)
			addException(e);
	}

	public boolean isConveyorRunning() {
		return this.bConveyorRunning;
	}

	public void clearFail() {
		if(errors.isEmpty())
			return;
		
		new Thread(new Runnable() {

			@Override
			public void run() {
				if(plcCmdDispatcher != null)	
					plcCmdDispatcher.close();
				
				sleep(2000);
				
				init();
				
				sensorQueryData = -1;
				
				bPlatformError = true;
				
				sleep(2000);
				
				if(!bPlatformError) {
					removeSystemException();
					
					logger.info("Clear connection error success");
				}else
					logger.info("clear connection error fail");
			}})
		.start();
	}

	private void removeSystemException() {
		removeException(new Bzpa1Exception(Reason.CONVEYOR_BROKEN));
		removeException(new Bzpa1Exception(Reason.PLC_OPEN_FAILED));
		removeException(new Bzpa1Exception(Reason.PLC_INIT_FAILED));
		removeException(new Bzpa1Exception(Reason.PLC_OUTPUT_CMD_ERROR));
		removeException(new Bzpa1Exception(Reason.PLC_ABNORMAL_FORMAT));
		removeException(new Bzpa1Exception(Reason.PLC_IO_ERROR));
		removeException(new Bzpa1Exception(Reason.PLC_LOGIC_ERROR));
		removeException(new Bzpa1Exception(Reason.PLC_CMD_TIMEOUT));
		removeException(new Bzpa1Exception(Reason.GRAB_TIMEOUT));
	}
	
	private void sleep(long time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void cbCmdResponse(String requestId, int cmdIdx, IPCCmdStatusType cmdType) {
		// TODO Auto-generated method stub
		
	}
	

}
