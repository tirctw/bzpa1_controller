package tirc.bzpa1.bzpa1_controller.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import tirc.bzpa1.bzpa1_controller.model.Bzpa1Exception;
import tirc.bzpa1.bzpa1_controller.model.Bzpa1Exception.Reason;
import tirc.bzpa1.bzpa1_controller.model.ECategory;
import tirc.bzpa1.bzpa1_controller.model.EFace;
import tirc.bzpa1.bzpa1_controller.model.EIncrease;
import tirc.bzpa1.bzpa1_controller.model.ELevel;
import tirc.bzpa1.bzpa1_controller.model.ELight;
import tirc.bzpa1.bzpa1_controller.model.EPrinterStatus;
import tirc.bzpa1.bzpa1_controller.model.ESystemStatus;
import tirc.bzpa1.bzpa1_controller.model.InkInfo;
import tirc.bzpa1.bzpa1_controller.model.PlatformStatus;
import tirc.bzpa1.bzpa1_controller.model.PlcEventHandler;
import tirc.bzpa1.bzpa1_controller.model.PrintConfiguration;
import tirc.bzpa1.bzpa1_controller.model.PrintSetting;
import tirc.bzpa1.bzpa1_controller.model.Settings;
import tirc.bzpa1.bzpa1_controller.model.SystemInfo;
import tirc.bzpa1.bzpa1_controller.repository.Profile;
import tirc.bzpa1.bzpa1_controller.repository.ProfileVariable;

@Service
public class SystemManagerService implements PlcEventHandler{	
	@Autowired PrinterService printerService;
	@Autowired PlcService plcService;
	@Autowired LogService logService;
	@Autowired CreatePDFService createPDFService;
	@Autowired PrinterRuntimeAssistant printerRuntimeAssistant;
	@Autowired ProfileService profileService;
	
	private static final Logger logger = LoggerFactory.getLogger(SystemManagerService.class);
	private long startClearFailTime = 0; 
	SystemInfo systemInfo = new SystemInfo();
	private boolean bPrintInProgress = false;
	private Settings settings = SettingService.getInstance().read();
	private boolean bOffEngine = false;
	private int appliedProfileTotalNum = 0;
	private EFace nowFace; 
	private boolean printWithAppliedProfile = false;
	private boolean simulated = false;
	
	@PostConstruct
	private void init() {
		if(!simulated) {
			plcService.init();
			plcService.setEventHandler(this);
		}
	}
	
	@Scheduled(fixedDelay = 500, initialDelay = 1000)
	public void updateAppliedProfile() throws IOException {		
		String jobInfo = printerService.getJobInfo();
		
		if(jobInfo.equals("")) {
			if(!simulated)
				return;
		}else
			logger.debug("Print progress : {}", jobInfo);	
		
		Profile p = profileService.getAppliedProfile();
		int printNum = printerService.getNowPrintNumber();
		
		if(printWithAppliedProfile && p != null && printNum != 0) {
			if(nowFace == EFace.FRONT) {
				if(p.getTotalPrintNumber() != appliedProfileTotalNum + printNum) {
					p.setTotalPrintNumber(appliedProfileTotalNum + printNum);
					profileService.updatePrintNum(p);
					logger.debug("printNum:{}", printNum);
				}
			}
		}
	}
	
	@Scheduled(fixedDelay = 1000, initialDelay = 1000)
	public void updateSystemStatus() {			
		if(bOffEngine || printerService.getPrinterStatus() == EPrinterStatus.OFF) {
			systemInfo.setStatus(ESystemStatus.OFF);
			bOffEngine = true;
		}
		/*需判斷printer 及 plc是否有錯誤 */
		else if(!printerService.isExceptionHappen() && !plcService.isExceptionHappen()) {
			if(printerService.getPrinterStatus() == EPrinterStatus.INITIALISING)
				systemInfo.setStatus(ESystemStatus.INIT);
			else if(printerService.getPrinterStatus() == EPrinterStatus.PRIMED_IDLE)
				systemInfo.setStatus(ESystemStatus.IDLE);
			else if(printerService.getPrinterStatus() == EPrinterStatus.PRINTING)
				systemInfo.setStatus(ESystemStatus.PRINTING);
			else if(printerService.getPrinterStatus() == EPrinterStatus.DEPRIMED_IDLE)
				systemInfo.setStatus(ESystemStatus.DEPRIMED);
			else
				systemInfo.setStatus(ESystemStatus.SERVING);
			
			systemInfo.setExceptions(new ArrayList<Bzpa1Exception>());
		}else  {
			boolean bClearFailTimeout = false;
			
			if(printerService.getErrors().isEmpty() && plcService.getErrors().isEmpty()) {
				systemInfo.setStatus(ESystemStatus.WARN);
			}
			else {
				if(systemInfo.getStatus() == ESystemStatus.CLEARING_FAIL && System.currentTimeMillis() - startClearFailTime > settings.getClearFailTimeout()) {
					bClearFailTimeout = true;
					systemInfo.setStatus(ESystemStatus.ERROR);
				}else if(systemInfo.getStatus() != ESystemStatus.CLEARING_FAIL) {
					systemInfo.setStatus(ESystemStatus.ERROR);
				}
			}
			mergeException(printerService.getErrors(), printerService.getWarns(), plcService.getErrors(), plcService.getWarns());
			
			if(bClearFailTimeout) 
				addException(new Bzpa1Exception(Reason.CLEARING_FAIL_TIMEOUT));
			
			stopPrint();
		}
		
		setIndicatorLight(systemInfo.getStatus());
	}
	private void addException(Bzpa1Exception e) {
		Collection<Bzpa1Exception> exceptions = systemInfo.getExceptions();
		
		for(Bzpa1Exception exception : exceptions) {
			if(exception.getCode().equals(e.getCode())) {
				return;
			}
		}
		exceptions.add(e);
	}
	
	private void mergeException(Map<String, Bzpa1Exception>... maps) {
		List<Bzpa1Exception> exceptions = new ArrayList<Bzpa1Exception>();

		for(Map<String, Bzpa1Exception> map : maps) {
		
			for(Bzpa1Exception ex : map.values()) {
				ex.setSolution(ex.getSolution() +  printerService.getJobInfo() );
				exceptions.add(ex);
			}
		}
		systemInfo.setExceptions(exceptions);
	}
	
	private void setIndicatorLight(ESystemStatus status) {			
		switch(status) {
		case IDLE:
		case SERVING:
		case PRINTING:
		case INIT:
			if(isPrinterWariningVal())
				plcService.setIndicatorLight(ELight.YELLOW);
			else
				plcService.setIndicatorLight(ELight.GREEN);
			break;
		case WARN:
				plcService.setIndicatorLight(ELight.YELLOW);
			break;
		case ERROR:
		case CLEARING_FAIL:
				plcService.setIndicatorLight(ELight.RED);
			break;
		case OFF:
				plcService.setIndicatorLight(ELight.NONE);
				break;
		default:
			plcService.setIndicatorLight(ELight.NONE);
			break;
		}
	}
	private boolean isPrinterWariningVal() {
		InkInfo info = systemInfo.getInk();
		float val = settings.getPrinterWarningVal();
		
		return (info.getC() < val || info.getM() < val || info.getY() < val || info.getK() < val || systemInfo.getWiper() < val);
	}
	
	@Scheduled(fixedDelay = 10000, initialDelay = 1000)
	public void updatePrinterStatus() {	
		systemInfo.setInk(printerService.getInkInfo());
		systemInfo.setPrinterConnected(printerService.getPrinterConnected());
		systemInfo.setWiper(printerService.getWiper());
		systemInfo.setMainWinRunning(printerRuntimeAssistant.isMainWinRunning());
		systemInfo.setTftpdRunning(printerRuntimeAssistant.isTftpdRunning());
		systemInfo.setPrinterPingable(printerRuntimeAssistant.isPrinterPingable());
	}
	
	@Scheduled(fixedDelay = 500, initialDelay = 1000)
	public void updatePlcStatus() {
		systemInfo.setPlatformHealth(!plcService.isExceptionHappen());
		systemInfo.setPrintNowPage(plcService.getPrintNowPage());
		systemInfo.setPrintTotalPage(plcService.getPrintTotalPage());
	}
	
	public SystemInfo getSystemInfo() {
		return systemInfo;
	}
	
	public void quickPrint(PrintConfiguration configuration, MultipartFile file) throws Exception{
		File tmpFile = File.createTempFile("bzpa1", ".tmp");
		FileUtils.copyInputStreamToFile(file.getInputStream(), tmpFile);
		
		configuration.setFilePath(tmpFile.getAbsolutePath());
		
		printWithAppliedProfile = false;
		
		print(configuration);
		
		tmpFile.delete();
	}
	
	public void preview(PrintConfiguration configuration, MultipartFile file) throws Exception{
		File tmpFile = null;
		
		if(file == null) {
			configuration.setFilePath(settings.getPDFPath() + configuration.getProfileName() + "\\" + configuration.getFilePath());
		}else {			
			tmpFile = File.createTempFile("bzpa1", ".tmp");
			FileUtils.copyInputStreamToFile(file.getInputStream(), tmpFile);
			
			configuration.setFilePath(tmpFile.getAbsolutePath());
		}
		createPDFService.generatePrintPDF(configuration, settings.getPreviewTargetFile());
		
		if (null != tmpFile) tmpFile.delete();
	}
	
	private void print(PrintConfiguration configuration) throws Exception{
		if(systemInfo.getStatus() == ESystemStatus.IDLE){
			int copies = configuration.getSettings().getCopies();
			checkPlatformReady();
			
			bPrintInProgress = true;
			
			String targetPath = createPDFService.generatePrintPDF(configuration, settings.getPrintTargetFile());
			
			printerService.print(targetPath, 1, 1);

			plcService.resetPrintPage(copies);

			logService.create(ECategory.SYSTEM, ELevel.INFO, "列印檔案 " + configuration.getFilePath() + " " + copies +"份");
			
			startPrintProcedure(copies);
		}else {
			String msg = "Now systemStatus \""+ systemInfo.getStatus()  + "\" is not status for printing.";
			logger.info(msg);
			throw new Bzpa1Exception(Reason.NOT_IDLE_STATUS);
		}
	}
	public void checkPlatformReady() {
		if(simulated)
			return;
		
		PlatformStatus status = plcService.getPlatformStatus();
		
		if(bPrintInProgress) 
			throw new Bzpa1Exception(Reason.PRINT_IN_PROGRESS);
		else if(status.isShortage()) 
			throw new Bzpa1Exception(Reason.BOX_SHORTAGE);
		else if(status.isStuck())
			throw new Bzpa1Exception(Reason.BOX_STUCK);
		else if(status.isDischarge())
			throw new Bzpa1Exception(Reason.DISCHARGE_MODE);
	}
	private void startPrintProcedure(int copies) {
		if(simulated)
			return;
		
		new Thread(() ->{
			while(printerService.getPrinterStatus() != EPrinterStatus.PRINTING)
				sleep(100);
				
			plcService.startPrintProcedure(copies);
		}).start();
	}
	private void sleep(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public void clearFail() {
		logger.debug("execute clearFail");
		
		if(!plcService.getErrors().isEmpty() || !printerService.getErrors().isEmpty()) {
			if(!plcService.getErrors().isEmpty())
				plcService.clearFail();
			
			if(!printerService.getErrors().isEmpty()) {
				if(printerService.getPrinterStatus() == EPrinterStatus.PRIMED_IDLE || 
						printerService.getPrinterStatus() == EPrinterStatus.FAULT)
					printerService.clearFail();
				else
					throw new Bzpa1Exception(Reason.NOT_PRINTER_IDLE_STATUS);
			}
			systemInfo.setStatus(ESystemStatus.CLEARING_FAIL);
			startClearFailTime = System.currentTimeMillis();
		}
	}

	@Override
	public void printFinished() {
		bPrintInProgress = false;
//		logger.info("print finish.");
	}
	
	public void engineShutdown() {
		printerService.engineShutDown();
		
		sleep(3000);
		
		printerService.endProgram();
	}
	
	public void printAppliedProfile(EFace face) throws Exception {
		printWithAppliedProfile = true;
		
		Profile profile = profileService.getAppliedProfile();
		
		if(profile == null)
			throw new FileNotFoundException();
		
		
		nowFace = face;
		appliedProfileTotalNum = profile.getTotalPrintNumber();
		
		PrintConfiguration c = new PrintConfiguration();
		String prefix = settings.getPDFPath() + profile.getName() + "\\";
		c.setFilePath(prefix + (face==EFace.FRONT ? profile.getFrontFile():profile.getBackFile()) );
		c.setProfileName(profile.getName());
		
		List<ProfileVariable> variables = new ArrayList<ProfileVariable>();
		
		for(ProfileVariable v:profile.getVariables()) {
			ProfileVariable v1 = new ProfileVariable();
			v1.setKey(v.getKey());
			v1.setName(v.getName());
			v1.setProfileId(v.getProfileId());
			v1.setFace(EFace.ALL);
			v1.setFormat(v.getFormat());
			
			if(v.getIncOp() != EIncrease.NONE) {
				if(face == EFace.FRONT) {
					v1.setIncOp(EIncrease.INCREASE);
					v1.setValue(String.valueOf((Integer.valueOf(v.getValue()) + appliedProfileTotalNum)));
				}else {
					v1.setIncOp(EIncrease.DECREASE);
					v1.setValue(String.valueOf((Integer.valueOf(v.getValue()) + appliedProfileTotalNum -1 )));
				}
			}else {
				v1.setValue(v.getValue());
			}
			variables.add(v1);
		}

		logger.debug(new Gson().toJson(variables));
		
		c.setVariables(variables);
		
		PrintSetting s = new PrintSetting();
		
		s.setCopies(profile.getCopies());
		s.setOffset(face==EFace.FRONT ? profile.getFrontOffset() : profile.getBackOffset());
		s.setSize(face==EFace.FRONT ? profile.getFrontSize() : profile.getBackSize());
		
		c.setSettings(s);
		c.setGrayScale(profile.isGrayScale());
		
		print(c);
		
		if(simulated)
			printerService.simulatePrint(s.getCopies());
	}

	@Transactional
	public Profile updateProfile(Profile profile, MultipartFile file1, MultipartFile file2) throws IOException {
		if(systemInfo.getStatus() == ESystemStatus.PRINTING)
			throw new Bzpa1Exception(Reason.PRINT_IN_PROGRESS);
		else
			return profileService.update(profile, file1, file2);
	}

	public void setAppliedProfileId(Long id) throws FileNotFoundException {
		if(systemInfo.getStatus() == ESystemStatus.IDLE)
			profileService.setAppliedId(id);
		else
			throw new Bzpa1Exception(Reason.NOT_IDLE_STATUS);
	}
	
	@Override
	public void stopPrint() {
		printerService.finishPrint();
		
		plcService.stopPrintProcedure();
		
		if(simulated)
			printerService.simulateStop();
		
		printFinished();
	}
	
}

