package tirc.bzpa1.bzpa1_controller.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.itextpdf.text.Document;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

import tirc.bzpa1.bzpa1_controller.model.EFace;
import tirc.bzpa1.bzpa1_controller.model.Settings;
import tirc.bzpa1.bzpa1_controller.repository.ProfileSize;

@Service
public class ScalePDFService {
	private static final Logger logger = LoggerFactory.getLogger(ScalePDFService.class);
	private Settings settings = SettingService.getInstance().read();
	
	//A4實際寬度210mm ，程式讀取寬度為595
	private final float DOT_PER_MM = 595.0f/210.0f;
	
	public static void main(String[] args) throws Exception {
		ScalePDFService service = new ScalePDFService();

		//651mm 297mm
		service.resize("C:\\bzpa1\\tmp.pdf", 326, 149, 0, null);
	}
	public ProfileSize getSize(String fileName) throws IOException {
		PdfReader reader = new PdfReader(fileName);
		Rectangle size = reader.getPageSize(1);
		float width = size.getWidth()/DOT_PER_MM;
		float height = size.getHeight()/DOT_PER_MM;
		
		return new ProfileSize((int)width, (int)height, null);
	}
	public void resize(String originFileName, int targetWidth, int targetHeight, int offset, String targetFileName) throws Exception{
		File originFile = new File(originFileName);
		String targetPath = originFile.getParent() + "\\" + targetFileName ;
		
		PdfReader reader = new PdfReader(new FileInputStream(originFileName));
		Rectangle originSize = reader.getPageSize(1);
		float originWidth = originSize.getWidth()/DOT_PER_MM;
		float originHeight = originSize.getHeight()/DOT_PER_MM;
		logger.info("w:{}, h:{}", originSize.getWidth()/DOT_PER_MM, originSize.getHeight()/DOT_PER_MM);
		
		float xScale =  targetWidth / originWidth;
		float yScale = targetHeight / originHeight;
		
		Rectangle printSize = new Rectangle(0, 0, settings.getPrintWidth() * DOT_PER_MM, targetHeight * DOT_PER_MM); 
		
		// Create output PDF
		Document doc = new Document(printSize);
		PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream(targetPath));
		
		doc.open();
		PdfContentByte cb = writer.getDirectContent();
		
		// Load existing PDF
		for(int i=1;i<=reader.getNumberOfPages();i++) {
			PdfImportedPage page = writer.getImportedPage(reader, i); 
			
			// Copy first page of existing PDF into output PDF
			doc.newPage();

			//指定座標以及縮放
			float resizeWidth = originSize.getWidth() * xScale;
			float resizeHeight = originSize.getHeight() * yScale;
			
			float newX = 0;
			float newY = 0;
		
			newX = (printSize.getWidth() - resizeWidth) / 2 + (offset * DOT_PER_MM);
			
//			logger.info("newSizeX:"+printSize.getWidth()+ ", originSizeX:"+originSize.getWidth()+", newX:"+newX);
//			logger.info("newSizeY:"+printSize.getHeight()+ ", originSizeY:"+originSize.getHeight()+", newY:"+newY);
			
			cb.addTemplate(page, xScale, 0, 0, yScale, newX, newY);
		}
		
		doc.close();
		writer.close();
		reader.close();
	}
}
