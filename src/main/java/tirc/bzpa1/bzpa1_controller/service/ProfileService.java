package tirc.bzpa1.bzpa1_controller.service;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.transaction.Transactional;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import tirc.bzpa1.bzpa1_controller.model.Bzpa1Exception;
import tirc.bzpa1.bzpa1_controller.model.Bzpa1Exception.Reason;
import tirc.bzpa1.bzpa1_controller.model.EFace;
import tirc.bzpa1.bzpa1_controller.model.EIncrease;
import tirc.bzpa1.bzpa1_controller.model.PDFInfo;
import tirc.bzpa1.bzpa1_controller.model.Settings;
import tirc.bzpa1.bzpa1_controller.repository.BoxVariable;
import tirc.bzpa1.bzpa1_controller.repository.Profile;
import tirc.bzpa1.bzpa1_controller.repository.ProfileRepository;
import tirc.bzpa1.bzpa1_controller.repository.ProfileSize;
import tirc.bzpa1.bzpa1_controller.repository.ProfileSizeRepository;
import tirc.bzpa1.bzpa1_controller.repository.ProfileVariable;
import tirc.bzpa1.bzpa1_controller.repository.ProfileVariableRepository;

@Service
public class ProfileService {
	@Autowired ProfileRepository profileDAO;
	@Autowired ProfileVariableRepository profileVariableDAO;
	@Autowired ProfileSizeRepository profileSizeDAO;
	@Autowired ScalePDFService scaleService;
	@Autowired CreatePDFService createPDFService; 
	
	private static final Logger logger = LoggerFactory.getLogger(ProfileService.class);
	private Settings settings = SettingService.getInstance().read();
	private Long appliedId;
	private Profile appliedProfile;
	
	public List<Profile> findAll() throws FileNotFoundException{
		List<Profile> profiles = new ArrayList<Profile>();
		
		for(Profile profile : profileDAO.findAll()) {
			profiles.add(findById(profile.getId()));
		}
		return profiles;
	}
	
	public Profile findById(long id) throws FileNotFoundException{
		Profile profile = profileDAO.findById(id).orElse(null);
		
		if(profile == null)
			return null;
		
		List<ProfileVariable> variables = profileVariableDAO.findByProfileIdAndFace(profile.getId(), EFace.ALL);
		List<ProfileVariable> frontVariables = profileVariableDAO.findByProfileIdAndFace(profile.getId(), EFace.FRONT);
		List<ProfileVariable> backVariables = profileVariableDAO.findByProfileIdAndFace(profile.getId(), EFace.BACK);
		
		ProfileSize frontSize = profileSizeDAO.findByProfileIdAndFace(profile.getId(), EFace.FRONT);
		ProfileSize backSize = profileSizeDAO.findByProfileIdAndFace(profile.getId(), EFace.BACK);
		
		profile.setVariables(variables);
		profile.setFrontVariables(frontVariables);
		profile.setBackVariables(backVariables);
		profile.setFrontSize(frontSize);
		profile.setBackSize(backSize);
		
		if( null != this.getAppliedId() && id == this.getAppliedId())
			profile.setApplied(true);
				
		return profile;
	};
	
	@Transactional
	public void updatePrintNum(Profile profile) throws IOException {
		profileDAO.save(profile);
		
		if(appliedId != null && appliedId.equals(profile.getId()))
			appliedProfile = profile;
	}
	
	private void copyFile(String profileName, MultipartFile file) throws IOException {
		
		if(file != null) {
			String file1Path =  settings.getPDFPath() + profileName + "\\" + file.getOriginalFilename();
			
			File tmpFile = File.createTempFile("bzpa1", ".tmp");
			FileUtils.copyInputStreamToFile(file.getInputStream(), tmpFile);
					
			File destFile = new File(file1Path);

			//比較檔案內容
			if (destFile.exists()) {
				
				try ( InputStream srcInputStream = Files.newInputStream(tmpFile.toPath());
					  InputStream destInputStream = Files.newInputStream(destFile.toPath())) {
					boolean result = IOUtils.contentEquals(srcInputStream, destInputStream);
					//若檔案內容一樣，就不用複製了
					if (result) {
						return;
					}
				} 
				
			}
			
			//若檔案內容不一樣，複製它
			FileUtils.copyFile(tmpFile, destFile);
			//刪除暫存檔
			tmpFile.delete();
		}
	}
	@Transactional
	public Profile update(Profile profile, MultipartFile file1, MultipartFile file2) throws IOException {
		String frontPath = settings.getPDFPath() + profile.getName() + "\\" + profile.getFrontFile();
		String backPath = settings.getPDFPath() + profile.getName() + "\\" + profile.getBackFile();
		ProfileSize frontSize = profile.getFrontSize();
		ProfileSize backSize = profile.getBackSize();
		
		if(frontSize == null)
			frontSize = scaleService.getSize(frontPath);
		
		if(backSize == null)
			backSize = scaleService.getSize(backPath);
		
		frontSize.setFace(EFace.FRONT);
		backSize.setFace(EFace.BACK);
		
		if(profile.getId() != null) {
			Profile foundProfile = profileDAO.findById(profile.getId()).orElse(null);
			
			//檢視profile name是否被修改，若被修改，則必須修改profile folder name
			if(foundProfile != null && !foundProfile.getName().equals(profile.getName())) {
				File oldFile = new File(settings.getPDFPath()+foundProfile.getName());
				File newFile = new File(settings.getPDFPath()+profile.getName());
				
				if(newFile.exists())
					throw new Bzpa1Exception(Reason.FILE_EXIST);
				
				oldFile.renameTo(newFile);
			}
			
			if(foundProfile != null) {
				profile.setId(foundProfile.getId());
				profileVariableDAO.deleteByProfileId(profile.getId());
				profileSizeDAO.deleteByProfileId(profile.getId());
			}
		}else {
			File file = new File(settings.getPDFPath() + profile.getName());
			
			file.mkdir();
		}
		copyFile(profile.getName(), file1);
		copyFile(profile.getName(), file2);
		
		Profile retProfile = profileDAO.save(profile);

		frontSize.setProfileId(retProfile.getId());
		backSize.setProfileId(retProfile.getId());

		retProfile.setFrontSize(profileSizeDAO.save(frontSize));
		retProfile.setBackSize(profileSizeDAO.save(backSize));

		List<ProfileVariable> retProfileVariables =  new ArrayList<ProfileVariable>();
		for(ProfileVariable v : profile.getVariables()) {
			v.setProfileId(retProfile.getId());
			v.setFace(EFace.ALL);
			retProfileVariables.add(profileVariableDAO.save(v));
		}
		
		List<ProfileVariable> retFrontVariables =  new ArrayList<ProfileVariable>();
		for(ProfileVariable v : profile.getFrontVariables()) {
			v.setProfileId(retProfile.getId());
			v.setFace(EFace.FRONT);
			retFrontVariables.add(profileVariableDAO.save(v));
		}
		
		List<ProfileVariable> retBackVariables =  new ArrayList<ProfileVariable>();
		for(ProfileVariable v : profile.getBackVariables()) {
			v.setProfileId(retProfile.getId());
			v.setFace(EFace.BACK);
			retBackVariables.add(profileVariableDAO.save(v));
		}
		retProfile.setVariables(retProfileVariables);
		retProfile.setFrontVariables(retFrontVariables);
		retProfile.setBackVariables(retBackVariables);
		
		if(appliedId != null && appliedId.equals(retProfile.getId()))
			appliedProfile = retProfile;
		
		try {
			File frontFile = new File(new File(frontPath).getParent(), "a.jpg");
			File backFile = new File(new File(frontPath).getParent(), "b.jpg");
			frontFile.delete();
			backFile.delete();
			snapshotPdf(new File(frontPath), frontFile);
			snapshotPdf(new File(backPath), backFile);
		} catch (Exception ex) {
			logger.error("failed to generate the thumbnails ", ex);
		}
		
		return retProfile;
	}
	
	private void snapshotPdf(File src, File dest) throws Exception{
		PDDocument doc = PDDocument.load(src);
		PDFRenderer renderer = new PDFRenderer(doc);
		BufferedImage inputImage = renderer.renderImageWithDPI(0, 300);
		
		doc.close();  //close file
		
		//BufferedImage image = renderer.renderImage(i, 2.5f);

		int scaledWidth = 300;
		int scaledHeight = 0;		
		scaledHeight = (int)(inputImage.getHeight() * ((float)scaledWidth)/(float)inputImage.getWidth() );
		
        // creates output image
        BufferedImage outputImage = new BufferedImage(
        		scaledWidth,
        		scaledHeight, 
        		inputImage.getType());
 
        // scales the input image to the output image
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();
 
		ImageIO.write(outputImage, "jpg", dest);
		
		
	}
	

	
	@Transactional
	public Profile deleteById(Long id) throws FileNotFoundException {

		Profile foundProfile = findById(id);
		
		if(id.equals(appliedId)) {
			this.appliedId = null;
			this.appliedProfile = null;
		}
		
		profileVariableDAO.deleteByProfileId(id);
		profileSizeDAO.deleteByProfileId(id);
		profileDAO.deleteById(id);
		
		return foundProfile;
	}

	public Long getAppliedId() {
		return appliedId;
	}

	public void setAppliedId(Long appliedId) throws FileNotFoundException {
		this.appliedId = appliedId;
		this.appliedProfile = findById(appliedId);
	}
	
	public Profile getAppliedProfile(){
		return appliedProfile;
	}
	
	@Transactional
    public List<Profile> refreshTotalProfiles() throws Exception{
        File[] profileFolders = new File(settings.getPDFPath()).listFiles();
        
        if(profileFolders != null){
        	List<Profile> profiles = findAll();

        	Set<String> origFileNames = new HashSet<String>();
        	
        	Set<String> newFileNames = new HashSet<String>();
        	Set<String> oldFileNames = new HashSet<String>();
        	
        	//記錄最新的profile
            for(File profileFolder:profileFolders){
            	if (profileFolder.isDirectory()) { //只需考慮目錄
            		newFileNames.add(profileFolder.getName().toUpperCase());
            		origFileNames.add(profileFolder.getName());  //保留原有目錄大小寫
            	}
            }
                        
            //比對最新profiles，先將不存在的profile刪除
            for(Profile p:profiles) {
            	
            	if(!newFileNames.contains(p.getName().toUpperCase())) {
            		deleteById(p.getId());
            	}else
            		oldFileNames.add(p.getName().toUpperCase());
            }
            
            logger.debug("old filenames: {}", oldFileNames);
            
            //比對舊有的profiles，新增profile
            for(String fileName:origFileNames) {
            	
            	logger.debug("fileName: {}", fileName);
            	
            	if(!oldFileNames.contains(fileName.toUpperCase())) {
            		//建立對應folder & 相關配置
            		createProfile(fileName);
            	}
            }
        }
        return findAll();
    }
    
    private Profile createProfile(String fileName) throws Exception {
    	String profilePath = settings.getPDFPath() + fileName + "\\";
    	Profile profile = new Profile();
    	
    	String frontFileName = findFileName(profilePath, "_正");
    	 //假如連1個pdf都找不到，回傳null
    	if (null==frontFileName) return null;  
    		
    	List<ProfileVariable> variables = new ArrayList<ProfileVariable>();
    	
    	if(frontFileName != null){
    		PDFInfo info = createPDFService.findInfo(profilePath + frontFileName);
        	profile.setFrontFile(frontFileName);
        	profile.setFrontSize(info.getSize());
        	profile.setFrontVariables(convert(info.getVariables()));
        	addVariables(variables, profile.getFrontVariables());
    	}
    	
    	
    	String backFileName = findFileName(profilePath, "_反");
    	
    	if(backFileName != null) {
    		PDFInfo info = createPDFService.findInfo(profilePath + backFileName);
        	profile.setBackFile(backFileName);
        	profile.setBackSize(info.getSize());
        	profile.setBackVariables(convert(info.getVariables()));
        	addVariables(variables, profile.getBackVariables());
    	}
    	
    	// 設定變數初始值
    	initVariables(variables);
    	
    	profile.setCopies(50);
    	profile.setName(fileName);
    	profile.setVariables(variables);

    	return update(profile, null, null);
    }
    
    private void initVariables(List<ProfileVariable> variables) {
    	for (ProfileVariable pv : variables) {
    		String value = "sample text";
    		EIncrease op = EIncrease.NONE;
    		if ("${CTN}".equalsIgnoreCase(pv.getKey())) {
    			value = "1";
    			op = EIncrease.INCREASE;
    		}
    		pv.setValue(value);
    		pv.setIncOp(op);
    		pv.setFormat("1");
    	}
    }
    
    
    private String findFileName(String pathName, String keyword) {
    	File[] files = new File(pathName).listFiles();
    	
    	List<File> pdfFiles = new ArrayList<>();
    	
    	if(files != null) {
    		for(File f : files) {
    			if (!FilenameUtils.isExtension(f.getName().toLowerCase(), "pdf")) continue;
    			pdfFiles.add(f);
    			if(f.getName().contains(keyword))
    				return f.getName();
    		}
    		if (!pdfFiles.isEmpty()) return pdfFiles.get(0).getName();
    	}

    	return null;
    }
    
    private List<ProfileVariable> convert(List<BoxVariable> boxVariables){
    	List<ProfileVariable> variables = new ArrayList<ProfileVariable>();
    	
    	for(BoxVariable v : boxVariables) {
    		ProfileVariable pv = new ProfileVariable(v.getKey(), v.getName(), null, EIncrease.NONE, null, null, null);
    		variables.add(pv);
    	}
    	
    	return variables;
    }
    
    private void addVariables(List<ProfileVariable> total, List<ProfileVariable> variables) throws CloneNotSupportedException {
    	Set<String> existedKeys = new HashSet<String>();
    	
    	for(ProfileVariable t:total) {
    		existedKeys.add(t.getKey());
    	}
    	
    	for(ProfileVariable v:variables){
    		if(!existedKeys.contains(v.getKey())) {
    			total.add((ProfileVariable) v.clone());
    		}
    	}
    }
    
    public Profile refreshById(Long profileId) throws Exception {
    	List<ProfileVariable> variables = new ArrayList<ProfileVariable>();
    	
    	Profile p = findById(profileId);
		PDFInfo info = createPDFService.findInfo(settings.getPDFPath() + p.getName() + "\\" + p.getFrontFile());
    	p.setFrontSize(info.getSize());
    	p.setFrontVariables(convert(info.getVariables()));
    	addVariables(variables, p.getFrontVariables());
    	
    	info = createPDFService.findInfo(settings.getPDFPath() + p.getName() + "\\" + p.getBackFile());
    	p.setBackSize(info.getSize());
    	p.setBackVariables(convert(info.getVariables()));
    	addVariables(variables, p.getBackVariables());
    	
    	// 設定變數初始值
    	initVariables(variables);
    	p.setVariables(variables);
    	
    	return p;
    }
}
