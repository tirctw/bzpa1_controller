package tirc.bzpa1.bzpa1_controller;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import tirc.bzpa1.bzpa1_controller.model.Settings;
import tirc.bzpa1.bzpa1_controller.service.SettingService;

@Configuration
public class SimpleWebAppConfigurer implements WebMvcConfigurer {

	//參考到外部pdf目錄
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		Settings settings = SettingService.getInstance().read();
		registry.addResourceHandler("/files/**")
				.addResourceLocations("file:////"+settings.getPDFPath());
	}
}