package tirc.bzpa1.bzpa1_controller.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import io.swagger.annotations.Api;
import tirc.bzpa1.bzpa1_controller.model.EPrinterConnected;
import tirc.bzpa1.bzpa1_controller.service.PrinterService;
import tirc.bzpa1.bzpa1_controller.service.SystemManagerService;

@Api(value = "PrinterController", description = "噴印機功能")
@RestController
@RequestMapping("/api/printer")
public class PrinterController {
	private static final Logger logger = LoggerFactory.getLogger(PrinterController.class);
	@Resource private SystemManagerService systemManagerService ;
	@Resource private PrinterService printerService ;
	private Gson gson = new Gson();
	
	@PutMapping(value = "/switch")
	public @ResponseBody ResponseEntity<?> setPrinterConnected(@RequestBody Map<String, Object> map) {
		String state = (String)map.get("switch");
		
		if(state.equals(EPrinterConnected.Online.name())) {
			logger.info("change to online");
			printerService.setToOnline();
		}
		else {
			logger.info("change to offline");
			printerService.setToOffline();
		}
		
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}  
	
	@PutMapping(value = "/abort")
	public @ResponseBody ResponseEntity<?> stopPrint() {
		systemManagerService.stopPrint();
		
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}  
	
	@PutMapping(value = "/reStartEng")
	public @ResponseBody ResponseEntity<?> reStartEng() {
		printerService.reStartEng();
		
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	} 
	
	@PutMapping(value = "/engineShutDown")
	public @ResponseBody ResponseEntity<?> engineShutDown() {
		systemManagerService.engineShutdown();
		
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}
	
	@PutMapping(value = "/endProgram")
	public @ResponseBody ResponseEntity<?> endProgram() {
		printerService.endProgram();
		
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}
	
}

