package tirc.bzpa1.bzpa1_controller.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import org.apache.commons.io.FileExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import tirc.bzpa1.bzpa1_controller.model.Bzpa1Exception;
import tirc.bzpa1.bzpa1_controller.service.SystemManagerService;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	private static final Logger logger = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);
	@Autowired SystemManagerService systemManagerService;
	
    @ExceptionHandler(value = { IllegalArgumentException.class, IllegalStateException.class })
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
        ex.printStackTrace();
        systemManagerService.printFinished();
        return handleExceptionInternal(ex, "[參數錯誤]"+ex.getMessage(), new HttpHeaders(), HttpStatus.CONFLICT, request);
    } 
    
    @ExceptionHandler(value = { FileNotFoundException.class })
    protected ResponseEntity<Object> handleFileNotFound(FileNotFoundException ex, WebRequest request) {
        ex.printStackTrace();
        systemManagerService.printFinished();
        return handleExceptionInternal(ex, "[檔案不存在]"+ex.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }
    
    @ExceptionHandler(value = { FileExistsException.class })
    protected ResponseEntity<Object> handleFileExists(FileExistsException ex, WebRequest request) {
        ex.printStackTrace();
        systemManagerService.printFinished();
        return handleExceptionInternal(ex, "[檔案已存在]", new HttpHeaders(), HttpStatus.METHOD_NOT_ALLOWED, request);
    }
    
    @ExceptionHandler(value = { IOException.class })
    protected ResponseEntity<Object> handleIOException(IOException ex, WebRequest request) {
        ex.printStackTrace();
        systemManagerService.printFinished();
        return handleExceptionInternal(ex, "[I/O錯誤]"+ex.getMessage(), new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }
    
    @ExceptionHandler(value = { ParseException.class })
    protected ResponseEntity<Object> handleParseException(ParseException ex, WebRequest request) {
        ex.printStackTrace();
        systemManagerService.printFinished();
        return handleExceptionInternal(ex, "[格式錯誤]"+ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
  
    @ExceptionHandler(value = { SQLException.class })
    protected ResponseEntity<Object> handleSQLException(SQLException ex, WebRequest request) {
        ex.printStackTrace();
        systemManagerService.printFinished();
        return handleExceptionInternal(ex, "[資料錯誤]請檢查輸入資料是否有誤或重複", new HttpHeaders(), HttpStatus.METHOD_NOT_ALLOWED, request);
    }
    
    @ExceptionHandler(value = { Bzpa1Exception.class })
    protected ResponseEntity<Object> handleBzpa1Exception(Bzpa1Exception ex, WebRequest request) {
        ex.printStackTrace();
        systemManagerService.printFinished();
        return handleExceptionInternal(ex, "["+ex.getCode()+"]"+ex.getMessage(), new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }
}
