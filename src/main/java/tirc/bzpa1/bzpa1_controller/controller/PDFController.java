package tirc.bzpa1.bzpa1_controller.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import tirc.bzpa1.bzpa1_controller.model.EFace;
import tirc.bzpa1.bzpa1_controller.model.PDFInfo;
import tirc.bzpa1.bzpa1_controller.model.PrintConfiguration;
import tirc.bzpa1.bzpa1_controller.model.Settings;
import tirc.bzpa1.bzpa1_controller.service.CreatePDFService;
import tirc.bzpa1.bzpa1_controller.service.SettingService;
import tirc.bzpa1.bzpa1_controller.service.SystemManagerService;

@Api(value = "PDF Controller", description = "PDF應用程式")
@RestController
@RequestMapping("/api/pdf")
public class PDFController {
	private static final Logger logger = LoggerFactory.getLogger(PDFController.class);
	@Autowired CreatePDFService pdfService;
	@Autowired SystemManagerService systemManagerService; 
	
	private Settings settings = SettingService.getInstance().read();
	private Gson gson = new Gson();
	
	@PostMapping(value = "/curProfile/printFront")
	public @ResponseBody ResponseEntity<?> printFront() throws Exception {
		systemManagerService.printAppliedProfile(EFace.FRONT);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}  
	
	@PostMapping(value = "/curProfile/printBack")
	public @ResponseBody ResponseEntity<?> printBack() throws Exception {
		systemManagerService.printAppliedProfile(EFace.BACK);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}  
	
	@ApiOperation(value="findInfo", notes="find info in existing pdf")
	@PostMapping(value = "/info")
	public @ResponseBody ResponseEntity<PDFInfo> findInfo(@RequestParam("pdfFile") MultipartFile file) throws IOException {		
		File tmpFile = File.createTempFile("bzpa1", ".tmp");
		FileUtils.copyInputStreamToFile(file.getInputStream(), tmpFile);
		
	    PDFInfo info = pdfService.findInfo(tmpFile.getAbsolutePath());
		return new ResponseEntity<PDFInfo>(info, HttpStatus.OK);
	}
	
	@ApiOperation(value="print", notes="print pdf with configuration")
	@PostMapping(value = "/print")
	public @ResponseBody ResponseEntity<?> print( 
			@RequestPart("config") PrintConfiguration configuration, 
			@RequestPart("pdfFile") MultipartFile file ) throws Exception 
	{
		systemManagerService.quickPrint(configuration, file);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	} 
	
	@PostMapping(value = "/preview")
	public @ResponseBody ResponseEntity<?> preview( 
			@RequestPart("config") PrintConfiguration configuration, 
			@RequestPart(value="pdfFile",  required=false) MultipartFile file ) throws Exception {		
		systemManagerService.preview(configuration, file);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	} 
}
