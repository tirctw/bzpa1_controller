package tirc.bzpa1.bzpa1_controller.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import io.swagger.annotations.Api;
import tirc.bzpa1.bzpa1_controller.model.PrintConfiguration;
import tirc.bzpa1.bzpa1_controller.repository.Profile;
import tirc.bzpa1.bzpa1_controller.service.ProfileService;
import tirc.bzpa1.bzpa1_controller.service.SystemManagerService;

@Api(value = "Profile Controller", description = "Profile應用程式")
@RestController
@RequestMapping("/api/profile")
public class ProfileController {
	private static final Logger logger = LoggerFactory.getLogger(ProfileController.class);
	@Resource private ProfileService profileService ;
	@Resource private SystemManagerService systemManagerService;
	
	private Gson gson = new Gson();
	
	@GetMapping(value = "/{id}")
	public @ResponseBody ResponseEntity<Profile> findById(@PathVariable long id) throws FileNotFoundException {
		Profile p = profileService.findById(id);
		return new ResponseEntity<Profile>(p, HttpStatus.OK);
	}
	
	@PutMapping(value = "")
	public @ResponseBody ResponseEntity<Profile> update( 
			@RequestPart("profile") Profile profile, 
			@RequestPart(value="pdfFile1", required=false) MultipartFile file1,
			@RequestPart(value="pdfFile2", required=false) MultipartFile file2) throws IOException{
		return new ResponseEntity<Profile>(systemManagerService.updateProfile(profile, file1, file2), HttpStatus.OK);
	}  
	
	@PutMapping(value = "/curProfileId")
	public @ResponseBody ResponseEntity<?> updateCurProfileId(@RequestBody Map<String, Object> map) throws FileNotFoundException {
		Long id = Long.valueOf((String)map.get("id"));
		systemManagerService.setAppliedProfileId(id);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}
	
	@GetMapping(value = "")
	public @ResponseBody ResponseEntity<List<Profile>> findAll() throws FileNotFoundException {
		return new ResponseEntity<List<Profile>>(profileService.findAll(), HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}")
	public @ResponseBody ResponseEntity<Profile> deleteById(@PathVariable long id) throws FileNotFoundException {
		Profile retProfile = profileService.deleteById(id);
		return new ResponseEntity<Profile>(retProfile, HttpStatus.OK);
	}  
	
	@GetMapping(value = "/curProfile")
	public @ResponseBody ResponseEntity<Profile> findCurProfile(){
		Profile p = profileService.getAppliedProfile();
		return new ResponseEntity<Profile>(p, HttpStatus.OK);
	}
	
	@GetMapping(value = "/refresh")
	public @ResponseBody ResponseEntity<?> refreshTotalProfiles() throws Exception {
		return new ResponseEntity<List<Profile>>(profileService.refreshTotalProfiles(), HttpStatus.OK);
	} 
	
	@GetMapping(value = "/refresh/{id}")
	public @ResponseBody ResponseEntity<?> refreshById(@PathVariable long id) throws Exception {
		return new ResponseEntity<Profile>(profileService.refreshById(id), HttpStatus.OK);
	} 
}

