package tirc.bzpa1.bzpa1_controller.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import io.swagger.annotations.Api;
import tirc.bzpa1.bzpa1_controller.model.SystemInfo;
import tirc.bzpa1.bzpa1_controller.service.SystemManagerService;

@Api(value = "System controller", description = "系統管理程式")
@RestController
@RequestMapping("/api/system")
public class SystemController {
	private static final Logger logger = LoggerFactory.getLogger(SystemController.class);
	@Autowired SystemManagerService managerService;
	private Gson gson = new Gson();
	

	@GetMapping(value = "/info")
	public @ResponseBody ResponseEntity<SystemInfo> findInfo(){		
		return new ResponseEntity<SystemInfo>(managerService.getSystemInfo(), HttpStatus.OK);
	}

	@PutMapping(value = "/clearFail")
	public @ResponseBody ResponseEntity<?> clearFail(){		
		managerService.clearFail();
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

}
