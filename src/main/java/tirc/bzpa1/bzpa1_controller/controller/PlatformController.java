package tirc.bzpa1.bzpa1_controller.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import io.swagger.annotations.Api;
import tirc.bzpa1.bzpa1_controller.model.EConveyorStatus;
import tirc.bzpa1.bzpa1_controller.service.PlcService;

@Api(value = "PlatformController", description = "平台相關功能")
@RestController
@RequestMapping("/api/platform")
public class PlatformController {
	private static final Logger logger = LoggerFactory.getLogger(PlatformController.class);
	@Resource private PlcService plcService ;
	private Gson gson = new Gson();
	
	@GetMapping(value = "/status")
	public @ResponseBody ResponseEntity<?> getPlatformStatus() {
		return new ResponseEntity<>(plcService.getPlatformStatus() , HttpStatus.OK);
	}  
	
	@PutMapping(value = "/conveyor")
		public @ResponseBody ResponseEntity<?> changeConveyorStatus(@RequestBody Map<String, Object> map) {
		String state = (String)map.get("state");
		
		if(state.equals("1")) {
			logger.debug("conveyor run");
			plcService.setConveyor(EConveyorStatus.RUN);
		}else {
			logger.debug("conveyor stop");
			plcService.setConveyor(EConveyorStatus.STOP);
		}
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}  
	
	@PutMapping(value = "/grabber")
	public @ResponseBody ResponseEntity<?> changeGrabberState(@RequestBody Map<String, Object> map) {
		String state = (String)map.get("state");
		
		if(state.equals("once")) {
			logger.debug("grabBox");
			plcService.grabAction();
		}
		else if(state.equals("loop")) {
			logger.debug("grabBoxRepeatedly");
			plcService.grabBoxRepeatedly();
		}
		else if(state.equals("stop")) {
			logger.debug("stopGrab");
			plcService.stopGrab();
		}
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}  
	
	@PutMapping(value = "/printProcedure")
	public @ResponseBody ResponseEntity<?> printProcedure(@RequestBody Map<String, Object> map) {
		String state = (String)map.get("state");
		
		if(state.equals("1")) {
			logger.debug("startPrintProcedure");
			plcService.resetPrintPage(Integer.MAX_VALUE);
			plcService.startPrintProcedure(Integer.MAX_VALUE);
		}
		else {
			logger.debug("stopPrintProcedure");
			plcService.stopPrintProcedure();
		}
		
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}  
}