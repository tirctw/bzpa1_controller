package tirc.bzpa1.bzpa1_controller.controller;
import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tirc.bzpa1.bzpa1_controller.model.ECategory;
import tirc.bzpa1.bzpa1_controller.repository.Log;
import tirc.bzpa1.bzpa1_controller.service.LogService;


@RestController
@RequestMapping("/api/log")
public class LogController {
	private Logger logger = LoggerFactory.getLogger( LogController.class ) ;
	@Autowired private LogService logService;
	
	@GetMapping(value = "")
	public @ResponseBody  ResponseEntity<List<Log>> findByFilter(@RequestParam("fromDate") String fromDate, @RequestParam("toDate") String toDate,
			@RequestParam("category") String category, @RequestParam("level") String level) throws ParseException{		
		return new ResponseEntity<List<Log>>(logService.findByFilter(level, category, fromDate, toDate), HttpStatus.OK);
	}

}
