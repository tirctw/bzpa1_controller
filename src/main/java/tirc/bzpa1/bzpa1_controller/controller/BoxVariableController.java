package tirc.bzpa1.bzpa1_controller.controller;

import java.io.FileNotFoundException;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import tirc.bzpa1.bzpa1_controller.repository.BoxVariable;
import tirc.bzpa1.bzpa1_controller.service.BoxVariableService;

@Api(value = "Box Variable Controller", description = "隨箱列印變數管理")
@RestController
@RequestMapping("/api/boxVariable")
public class BoxVariableController {
	private static final Logger logger = LoggerFactory.getLogger(BoxVariableController.class);
	@Resource private BoxVariableService boxVariableService ;
	private Gson gson = new Gson();
	
	@ApiOperation(value="update", notes="create/update variable")
    @ApiResponses(value = { 
            @ApiResponse(code = 200, message = "update success."),
            @ApiResponse(code = 401, message = "not authorized."), 
            @ApiResponse(code = 403, message = "forbidden."),
            @ApiResponse(code = 404, message = "not found.") })
	@PutMapping(value = "")
	public @ResponseBody ResponseEntity<BoxVariable> update(@RequestBody BoxVariable val) {
		logger.debug(gson.toJson(val));
		return new ResponseEntity<BoxVariable>(boxVariableService.update(val), HttpStatus.OK);
	}  
	
	
	@ApiOperation(value="findAll", notes="find all variables")
    @ApiResponses(value = { 
            @ApiResponse(code = 200, message = "find success."),
            @ApiResponse(code = 401, message = "not authorized."), 
            @ApiResponse(code = 403, message = "forbidden."),
            @ApiResponse(code = 404, message = "not found.") })
	@GetMapping(value = "")
	public @ResponseBody ResponseEntity<List<BoxVariable>> findAll() {
		return new ResponseEntity<List<BoxVariable>>(boxVariableService.findAll(), HttpStatus.OK);
	}
	
	@ApiOperation(value="deleteByKey", notes="delete variable by key")
    @ApiResponses(value = { 
            @ApiResponse(code = 200, message = "delete success."),
            @ApiResponse(code = 401, message = "not authorized."), 
            @ApiResponse(code = 403, message = "forbidden."),
            @ApiResponse(code = 404, message = "not found.") })
	@DeleteMapping(value = "/{key}")
	public @ResponseBody ResponseEntity<BoxVariable> deleteByKey(@PathVariable String key) throws FileNotFoundException {
		BoxVariable retObj = boxVariableService.deleteByKey(key);
		return new ResponseEntity<BoxVariable>(retObj, HttpStatus.OK);
	}  
}

