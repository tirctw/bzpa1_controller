package tirc.bzpa1.bzpa1_controller.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import tirc.bzpa1.bzpa1_controller.model.EFace;

@Repository
public interface ProfileVariableRepository extends CrudRepository<ProfileVariable, Long>{
	
	List<ProfileVariable> findByProfileIdAndFace(Long profileId, EFace face);
	
	void deleteByProfileId(Long profileId);
}
