package tirc.bzpa1.bzpa1_controller.repository;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tirc.bzpa1.bzpa1_controller.model.EFace;

@Entity
@Table(name = "profile_size")
public class ProfileSize {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private int width;
	private int height;
	@Column(name = "profile_id")
	private Long profileId;
	private EFace face;
	
	protected ProfileSize() {}
	
	public ProfileSize (int width, int height, EFace face){
		this.width = width;
		this.height = height;
		this.face = face;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}

	public EFace getFace() {
		return face;
	}

	public void setFace(EFace face) {
		this.face = face;
	}
}