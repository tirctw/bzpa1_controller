package tirc.bzpa1.bzpa1_controller.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import tirc.bzpa1.bzpa1_controller.model.EFace;

@Repository
public interface ProfileSizeRepository extends CrudRepository<ProfileSize, Long>{
	
	ProfileSize findByProfileIdAndFace(Long profileId, EFace face);
	
	void deleteByProfileId(Long profileId);
}
