package tirc.bzpa1.bzpa1_controller.repository;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tirc.bzpa1.bzpa1_controller.model.ECategory;
import tirc.bzpa1.bzpa1_controller.model.ELevel;

@Entity
@Table(name = "log")
public class Log {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private ECategory category;
	private ELevel level;
	
	@Column(length = 1024)
	private String message;
	private String createdTs;
	
	public Log() {}
	
	public Log(ECategory category, ELevel level, String message, String createdTs) {
		this.category = category;
		this.setLevel(level);
		this.message = message;
		this.createdTs = createdTs;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ECategory getCategory() {
		return category;
	}

	public void setCategory(ECategory category) {
		this.category = category;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCreatedTs() {
		return createdTs;
	}

	public void setCreatedTs(String createdTs) {
		this.createdTs = createdTs;
	}

	public ELevel getLevel() {
		return level;
	}

	public void setLevel(ELevel level) {
		this.level = level;
	}
}
