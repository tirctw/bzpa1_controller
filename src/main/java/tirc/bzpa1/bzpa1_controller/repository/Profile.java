package tirc.bzpa1.bzpa1_controller.repository;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Profile {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(unique=true)
	private String name;
	
	private String frontFile;
	private String backFile;
	
	private int frontOffset;
	private int backOffset;
	
	private int copies;
	private int totalPrintNumber;
	private int frontPrintNumber;
	private int backPrintNumber;
	private boolean grayScale;
	
	private transient ProfileSize frontSize;
	private transient ProfileSize backSize;
	private transient List<ProfileVariable> frontVariables;
	private transient List<ProfileVariable> backVariables;
	private transient List<ProfileVariable> variables;
	private transient boolean applied;
	
	public Profile() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFrontFile() {
		return frontFile;
	}

	public void setFrontFile(String frontFile) {
		this.frontFile = frontFile;
	}

	public String getBackFile() {
		return backFile;
	}

	public void setBackFile(String backFile) {
		this.backFile = backFile;
	}

	public int getFrontOffset() {
		return frontOffset;
	}

	public void setFrontOffset(int frontOffset) {
		this.frontOffset = frontOffset;
	}

	public int getBackOffset() {
		return backOffset;
	}

	public void setBackOffset(int backOffset) {
		this.backOffset = backOffset;
	}

	public int getCopies() {
		return copies;
	}

	public void setCopies(int copies) {
		this.copies = copies;
	}

	public int getTotalPrintNumber() {
		return totalPrintNumber;
	}

	public void setTotalPrintNumber(int totalPrintNumber) {
		this.totalPrintNumber = totalPrintNumber;
	}

	public int getFrontPrintNumber() {
		return frontPrintNumber;
	}

	public void setFrontPrintNumber(int frontPrintNumber) {
		this.frontPrintNumber = frontPrintNumber;
	}

	public int getBackPrintNumber() {
		return backPrintNumber;
	}

	public void setBackPrintNumber(int backPrintNumber) {
		this.backPrintNumber = backPrintNumber;
	}

	public ProfileSize getFrontSize() {
		return frontSize;
	}

	public void setFrontSize(ProfileSize frontSize) {
		this.frontSize = frontSize;
	}

	public ProfileSize getBackSize() {
		return backSize;
	}

	public void setBackSize(ProfileSize backSize) {
		this.backSize = backSize;
	}

	public List<ProfileVariable> getFrontVariables() {
		return frontVariables;
	}

	public void setFrontVariables(List<ProfileVariable> frontVariables) {
		this.frontVariables = frontVariables;
	}

	public List<ProfileVariable> getBackVariables() {
		return backVariables;
	}

	public void setBackVariables(List<ProfileVariable> backVariables) {
		this.backVariables = backVariables;
	}

	public List<ProfileVariable> getVariables() {
		return variables;
	}

	public void setVariables(List<ProfileVariable> variables) {
		this.variables = variables;
	}

	public boolean isApplied() {
		return applied;
	}

	public void setApplied(boolean applied) {
		this.applied = applied;
	}

	public boolean isGrayScale() {
		return grayScale;
	}

	public void setGrayScale(boolean grayScale) {
		this.grayScale = grayScale;
	}
}
