package tirc.bzpa1.bzpa1_controller.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BoxVariableRepository extends CrudRepository<BoxVariable, Long>{
	BoxVariable findByKey(String key);
}
