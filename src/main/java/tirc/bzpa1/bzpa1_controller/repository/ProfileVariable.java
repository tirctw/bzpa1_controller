package tirc.bzpa1.bzpa1_controller.repository;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import tirc.bzpa1.bzpa1_controller.model.EFace;
import tirc.bzpa1.bzpa1_controller.model.EIncrease;

@Entity
@Table(name = "profile_variable")
public class ProfileVariable implements Cloneable{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name = "key1")
	private String key;
	private String name;
	private String value;
	private EIncrease incOp = EIncrease.NONE;
	@Column(name = "profile_id")
	private Long profileId;
	private EFace face;
	private String format; 
	
	@Override
	public Object clone() throws CloneNotSupportedException {
	  return super.clone();
	}
	public ProfileVariable() {}
	
	public ProfileVariable(String key, String name, String value, EIncrease incOp, Long profileId, EFace face, String format) {
		this.key = key;
		this.name = name;
		this.value = value;
		this.setIncOp(incOp);
		this.profileId = profileId;
		this.face = face;
		this.format = format;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	
	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public EIncrease getIncOp() {
		return incOp;
	}

	public void setIncOp(EIncrease incOp) {
		this.incOp = incOp;
	}

	public EFace getFace() {
		return face;
	}

	public void setFace(EFace face) {
		this.face = face;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}
}
