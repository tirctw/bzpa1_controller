package tirc.bzpa1.bzpa1_controller.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import tirc.bzpa1.bzpa1_controller.model.ECategory;

@Repository
public interface LogRepository extends CrudRepository<Log, Long>{
	List<Log> findByCreatedTsBetween(String from, String to);
	
	List<Log> findByCategoryAndCreatedTsBetween(ECategory category, String from, String to);
}
