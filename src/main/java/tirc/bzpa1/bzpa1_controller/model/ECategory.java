package tirc.bzpa1.bzpa1_controller.model;

public enum ECategory {
	PRINTER(0), 
	PLATFORM(1),
	SYSTEM(2);
	
	private final int sn ;
	
	private ECategory( int sn ) {
		this.sn = sn ;
	}
	
	public int getSN() {
		return sn ;
	}
	
	public static ECategory parse( int sn ) {
		for( ECategory o : values() ) {
			if ( o.getSN() == sn )
				return o ;
		}
		throw new RuntimeException("parse error!");
	}
}
