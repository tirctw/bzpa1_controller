package tirc.bzpa1.bzpa1_controller.model;

public enum ELevel {
	DEBUG(0), 
	INFO(1),
	WARN(2),
	ERROR(3),
	FATAL(4);
	
	private final int sn ;
	
	private ELevel( int sn ) {
		this.sn = sn ;
	}
	
	public int getSN() {
		return sn ;
	}
	
	public static ELevel parse( int sn ) {
		for( ELevel o : values() ) {
			if ( o.getSN() == sn )
				return o ;
		}
		throw new RuntimeException("parse error!");
	}
}
