package tirc.bzpa1.bzpa1_controller.model;

public class Bzpa1Exception extends RuntimeException{  
	public enum Reason{
		PRINTER_HEAD_TOUCH_SWITCH(ELevel.WARN, ECategory.PRINTER , "E01", "打印頭接觸到限位開關", "請調整打印頭距離"),
		WASTE_INK_NOT_INSTALLED(ELevel.WARN, ECategory.PRINTER , "E02", "廢墨水盒未安裝", "請安裝廢墨水盒"),
		WASTE_INK_FULL(ELevel.WARN, ECategory.PRINTER , "E03", "廢墨水盒已滿", "請更換廢墨水盒"),
		PRINTER_EMERGENCY_STOP(ELevel.WARN, ECategory.PRINTER , "E04", "緊急停止", "排除錯誤後，請將緊急按鈕旋開"),
		PRINT_TIMEOUT(ELevel.ERROR, ECategory.PRINTER , "E05", "列印逾時", "請檢查紙箱是否短缺或輸送帶上是否有異物卡住"),
		WIPER_END_OF_LIFE(ELevel.WARN, ECategory.PRINTER , "E06", "Wiper使用壽命到了", "請更換Wiper"),
		ENGINE_SHUTDOWN_TIMEOUT(ELevel.FATAL, ECategory.PRINTER, "E07", "關閉噴印機逾時", "請直接關閉噴印機"),
		PRINTER_RESPONSE_FAIL(ELevel.FATAL, ECategory.PRINTER, "E08", "噴印機指令回應錯誤", "請檢查噴印機是否正常運行或狀態是否正確"),
		PRINTER_ENGINE_ERROR(ELevel.ERROR, ECategory.PRINTER , "E11", "噴印機內部錯誤", "噴印機內部錯誤，請回報廠商"),
		PRINTER_NOT_RESPONSE(ELevel.FATAL, ECategory.PRINTER , "E21", "噴印機目前無回應", "噴印機無回應，請檢查系統電源及網路線"),
		
		CONVEYOR_BROKEN(ELevel.FATAL, ECategory.PLATFORM, "E31", "輸送平台故障", "請依序執行噴印機關機及系統關機程序後，重啟總電源"),
		PLC_OPEN_FAILED(ELevel.FATAL, ECategory.PLATFORM, "E32", "PLC建立socket失敗", "請依序執行噴印機關機及系統關機程序後，重啟總電源"),
		PLC_INIT_FAILED(ELevel.FATAL, ECategory.PLATFORM, "E33", "PLC初始化失敗", "請依序執行噴印機關機及系統關機程序後，重啟總電源"),
		PLC_OUTPUT_CMD_ERROR(ELevel.FATAL, ECategory.PLATFORM, "E34", "PLC輸出指令錯誤", "請依序執行噴印機關機及系統關機程序後，重啟總電源"),
		PLC_ABNORMAL_FORMAT(ELevel.FATAL, ECategory.PLATFORM , "E35", "PLC Abnormal Format", "請依序執行噴印機關機及系統關機程序後，重啟總電源"),
		PLC_IO_ERROR(ELevel.FATAL, ECategory.PLATFORM , "E36", "PLC I/O Error", "請依序執行噴印機關機及系統關機程序後，重啟總電源"),
		PLC_LOGIC_ERROR(ELevel.FATAL, ECategory.PLATFORM , "E37", "PLC Logic Error", "請依序執行噴印機關機及系統關機程序後，重啟總電源"),
		PLC_CMD_TIMEOUT(ELevel.FATAL, ECategory.PLATFORM , "E38", "PLC 命令逾時", "請依序執行噴印機關機及系統關機程序後，重啟總電源"),
		PLC_EMERGENCY_STOP(ELevel.WARN, ECategory.PLATFORM , "E39", "PLC 緊急停止", "排除錯誤後，請將緊急按鈕旋開"),
		GRAB_TIMEOUT(ELevel.FATAL, ECategory.PLATFORM , "E40", "夾取箱子逾時", "請依序執行噴印機關機及系統關機程序後，重啟總電源"),
		BOX_SHORTAGE(ELevel.WARN, ECategory.PLATFORM , "E41", "入料區箱子短缺", "請補足箱子"),
		DISCHARGE_FULL(ELevel.WARN, ECategory.PLATFORM , "E42", "出料區箱子已滿", "請執行卸貨程序"),
		BOX_STUCK(ELevel.WARN, ECategory.PLATFORM , "E43", "入料區發生卡箱", "請移除入料區異物"),
		DISCHARGE_MODE(ELevel.WARN, ECategory.PLATFORM , "E44", "出料區目前為卸貨模式", "請切換為出料模式"),
		
		FONT_NOT_SUPPORT(ELevel.WARN, ECategory.SYSTEM, "E101", "目前使用中的字型不支援列印文字，請修改文字或更換使用字型", "請修改文字或更換使用字型"),
		NOT_IDLE_STATUS(ELevel.WARN, ECategory.SYSTEM, "E102", "僅IDLE狀態才允許該行為", "請檢查系統狀態是否為IDLE"),
		PRINT_IN_PROGRESS(ELevel.WARN, ECategory.SYSTEM, "E103", "列印正在進行中", "請稍後再執行"),
		CLEARING_FAIL_TIMEOUT(ELevel.FATAL, ECategory.SYSTEM, "E104", "清除錯誤失敗", "請依序執行噴印機關機及系統關機程序後，重啟總電源"),
		NOT_PRINTER_IDLE_STATUS(ELevel.WARN, ECategory.SYSTEM, "E105", "噴印機當前狀態不允許清除錯誤，請稍等兩分鐘後再執行", "請稍待再執行清除錯誤"),
		NO_BACK_PRINT_PAGE(ELevel.WARN, ECategory.SYSTEM, "E106", "無背面列印可對應的頁數", "請檢查設定是否有誤"),
		FILE_EXIST(ELevel.WARN, ECategory.SYSTEM, "E107", "檔案已存在", "請檢查設定是否有誤"),
		;
		
		private ELevel level;
		private ECategory category;
		private String code;
		private String message;
		private String solution;
		
		Reason(ELevel level, ECategory category, String code, String message, String solution) {
			this.level = level;
			this.category = category;
			this.code = code;
			this.message = message;
			this.solution = solution;
		}
		public ELevel getLevel() {
			return level;
		}
		public ECategory getCategory() {
			return category;
		}
		public String getCode() {
			return code;
		}
		public String getMessage() {
			return message;
		}
		public String getSolution() {
			return solution;
		}
		public static Reason parse( String code ) {
			for( Reason o : values() ) {
				if ( o.getCode().equals(code) )
					return o ;
			}
			throw new RuntimeException("parse error!");
		}
	}
	
	private Reason reason;
	private ECategory category;
	private ELevel level;
	private String code;
	private String message;
	private String solution;
	
	public Bzpa1Exception(Reason reason) {
		this.reason = reason;
		this.category = reason.getCategory();
		this.level = reason.getLevel();
		this.code = reason.getCode();
		this.message = reason.getMessage();
		this.solution = reason.getSolution();
	}
	
	public Reason getReason() {
		return reason;
	}

	public ECategory getCategory() {
		return category;
	}

	public ELevel getLevel() {
		return level;
	}
	
	public void setCategory(ECategory category) {
		this.category = category;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setReason(Reason reason) {
		this.reason = reason;
	}
	public String getSolution() {
		return solution;
	}
	public void setSolution(String solution) {
		this.solution = solution;
	}
}