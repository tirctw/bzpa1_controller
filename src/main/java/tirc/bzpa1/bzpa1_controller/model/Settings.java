package tirc.bzpa1.bzpa1_controller.model;

public class Settings {
	private String PDFPath = "C:\\bzpa1\\";
	private String printerAppIp = "127.0.0.1";
	private String printTargetFile = "printPDF.pdf";
	private String previewTargetFile = "preview.pdf";
	private float printWidth = 320;
	private int printerPort = 9102;
	private String plcId = "PLC1";
	private String plcIp = "192.168.2.3";
	private String plcPort = "5062";
	private int plcInputCmdIdxForConveyor = 1000;
	private int plcInputCmdIdxForGrabber = 1001;
	private String fontName = "";
	private String fontColor = "BLACK";
	private String fontPath = "C:\\WINDOWS\\FONTS\\";
	private String tftpdAppName = "tftpd64.exe";
	private String tftpdAppPath = "C:\\Program Files\\Tftpd64\\" + tftpdAppName;
	private String mainWinAppName = "MainWin.exe";
	private String mainWinAppPath = "C:\\MainWin\\" + mainWinAppName;	
	private String printerDeviceIp = "192.168.100.200";
	private int grabBoxTimeout = 10000;
	private int clearFailTimeout = 180000;
	private int engineShutdownTimeout = 120000; 
	private float printerWarningVal = 5.0f;
	
	public String getFontName() {
		return fontName;
	}
	public void setFontName(String fontName) {
		this.fontName = fontName;
	}
	public String getFontPath() {
		return fontPath;
	}
	public void setFontPath(String fontPath) {
		this.fontPath = fontPath;
	}
	public String getFontColor() {
		return fontColor;
	}
	public void setFontColor(String fontColor) {
		this.fontColor = fontColor;
	}
	public String getPlcId() {
		return plcId;
	}
	public void setPlcId(String plcId) {
		this.plcId = plcId;
	}
	public String getPlcPort() {
		return plcPort;
	}
	public void setPlcPort(String plcPort) {
		this.plcPort = plcPort;
	}
	public String getPDFPath() {
		return PDFPath;
	}
	public void setPDFPath(String pDFPath) {
		PDFPath = pDFPath;
	}
	public String getPrinterAppIp() {
		return printerAppIp;
	}
	public void setPrinterAppIp(String printerAppIp) {
		this.printerAppIp = printerAppIp;
	}
	public String getTftpdAppName() {
		return tftpdAppName;
	}
	public void setTftpdAppName(String tftpdAppName) {
		this.tftpdAppName = tftpdAppName;
	}
	public String getTftpdAppPath() {
		return tftpdAppPath;
	}
	public void setTftpdAppPath(String tftpdAppPath) {
		this.tftpdAppPath = tftpdAppPath;
	}
	public String getMainWinAppName() {
		return mainWinAppName;
	}
	public void setMainWinAppName(String mainWinAppName) {
		this.mainWinAppName = mainWinAppName;
	}
	public String getMainWinAppPath() {
		return mainWinAppPath;
	}
	public void setMainWinAppPath(String mainWinAppPath) {
		this.mainWinAppPath = mainWinAppPath;
	}
	public String getPrinterDeviceIp() {
		return printerDeviceIp;
	}
	public void setPrinterDeviceIp(String printerDeviceIp) {
		this.printerDeviceIp = printerDeviceIp;
	}
	public String getPrintTargetFile() {
		return printTargetFile;
	}
	public void setPrintTargetFile(String printTargetFile) {
		this.printTargetFile = printTargetFile;
	}
	public int getPrinterPort() {
		return printerPort;
	}
	public void setPrinterPort(int printerPort) {
		this.printerPort = printerPort;
	}
	public int getPlcInputCmdIdxForConveyor() {
		return plcInputCmdIdxForConveyor;
	}
	public void setPlcInputCmdIdxForConveyor(int plcInputCmdIdxForConveyor) {
		this.plcInputCmdIdxForConveyor = plcInputCmdIdxForConveyor;
	}
	public float getPrintWidth() {
		return printWidth;
	}
	public void setPrintWidth(float printWidth) {
		this.printWidth = printWidth;
	}
	public int getPlcInputCmdIdxForGrabber() {
		return plcInputCmdIdxForGrabber;
	}
	public void setPlcInputCmdIdxForGrabber(int plcInputCmdIdxForGrabber) {
		this.plcInputCmdIdxForGrabber = plcInputCmdIdxForGrabber;
	}
	public int getGrabBoxTimeout() {
		return grabBoxTimeout;
	}
	public void setGrabBoxTimeout(int grabBoxTimeout) {
		this.grabBoxTimeout = grabBoxTimeout;
	}
	public int getClearFailTimeout() {
		return clearFailTimeout;
	}
	public void setClearFailTimeout(int clearFailTimeout) {
		this.clearFailTimeout = clearFailTimeout;
	}
	public String getPlcIp() {
		return plcIp;
	}
	public void setPlcIp(String plcIp) {
		this.plcIp = plcIp;
	}
	public float getPrinterWarningVal() {
		return printerWarningVal;
	}
	public void setPrinterWarningVal(float printerWarningVal) {
		this.printerWarningVal = printerWarningVal;
	}
	public int getEngineShutdownTimeout() {
		return engineShutdownTimeout;
	}
	public void setEngineShutdownTimeout(int engineShutdownTimeout) {
		this.engineShutdownTimeout = engineShutdownTimeout;
	}
	public String getPreviewTargetFile() {
		return previewTargetFile;
	}
	public void setPreviewTargetFile(String previewTargetFile) {
		this.previewTargetFile = previewTargetFile;
	}
}
