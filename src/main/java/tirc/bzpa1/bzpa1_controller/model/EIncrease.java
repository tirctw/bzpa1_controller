package tirc.bzpa1.bzpa1_controller.model;

public enum EIncrease {
	NONE(0), 
	INCREASE(1),
	DECREASE(2);
	
	private final int sn ;
	
	private EIncrease( int sn ) {
		this.sn = sn ;
	}
	
	public int getSN() {
		return sn ;
	}
	
	public static EIncrease parse( int sn ) {
		for( EIncrease o : values() ) {
			if ( o.getSN() == sn )
				return o ;
		}
		throw new RuntimeException("parse error!");
	}
}
