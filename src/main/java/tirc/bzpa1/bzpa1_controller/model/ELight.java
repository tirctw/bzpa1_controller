package tirc.bzpa1.bzpa1_controller.model;

public enum ELight {
	NONE(0),
	RED(1),
	YELLOW(2),
	GREEN(4)
	;
	
	private final int sn ;
	private ELight( int sn ) {
		this.sn = sn ;
	}
	public int getSN() {
		return sn ;
	}
	public static ELight parse( int sn ) {
		for( ELight o : values() ) {
			if ( o.getSN() == sn )
				return o ;
		}
		throw new RuntimeException("parse error!");
	}
}
