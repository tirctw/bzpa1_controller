package tirc.bzpa1.bzpa1_controller.model;

public interface PlcEventHandler {
	void printFinished();
	
	void stopPrint();
}
