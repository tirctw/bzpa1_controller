package tirc.bzpa1.bzpa1_controller.model;

public enum ESystemStatus {
	IDLE(0), 
	SERVING(1),
	PRINTING(2),
	WARN(3),
	ERROR(4),
	CLEARING_FAIL(5),
	INIT(6),
	OFF(7),
	DEPRIMED(8)
	;
	
	private final int sn ;
	
	private ESystemStatus( int sn ) {
		this.sn = sn ;
	}
	
	public int getSN() {
		return sn ;
	}
	
	public static ESystemStatus parse( int sn ) {
		for( ESystemStatus o : values() ) {
			if ( o.getSN() == sn )
				return o ;
		}
		throw new RuntimeException("parse error!");
	}
}
