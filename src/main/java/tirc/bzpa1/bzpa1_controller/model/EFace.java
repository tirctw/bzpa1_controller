package tirc.bzpa1.bzpa1_controller.model;

public enum EFace {
	FRONT(0),
	BACK(1),
	ALL(2)
	;
	
	private final int sn ;
	private EFace( int sn ) {
		this.sn = sn ;
	}
	public int getSN() {
		return sn ;
	}
	public static EFace parse( int sn ) {
		for( EFace o : values() ) {
			if ( o.getSN() == sn )
				return o ;
		}
		throw new RuntimeException("parse error!");
	}
}
