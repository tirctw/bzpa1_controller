package tirc.bzpa1.bzpa1_controller.model;

import tirc.bzpa1.bzpa1_controller.repository.ProfileSize;

public class PrintSetting {
	
	private int offset = 0;
	private ProfileSize size;
	private int copies;

	public PrintSetting() {
		
	}
	public PrintSetting(int offset, ProfileSize size, int copies) {
		this.offset = offset;
		this.size = size;
		this.copies = copies;
	}
	public ProfileSize getSize() {
		return size;
	}
	public void setSize(ProfileSize size) {
		this.size = size;
	}
	public int getCopies() {
		return copies;
	}
	public void setCopies(int copies) {
		this.copies = copies;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
}
