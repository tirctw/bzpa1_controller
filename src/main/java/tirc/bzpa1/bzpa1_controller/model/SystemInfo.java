package tirc.bzpa1.bzpa1_controller.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SystemInfo {
	private EPrinterConnected printerConnected = EPrinterConnected.Offline;
	private InkInfo ink = new InkInfo();
	private float wiper = 0.0f;
	private ESystemStatus status = ESystemStatus.IDLE;
	private List<Bzpa1Exception> exceptions = new ArrayList<Bzpa1Exception>();
	private boolean tftpdRunning = false;
	private boolean mainWinRunning = false;
	private boolean printerPingable = false;
	private boolean platformHealth = false;
	private int printTotalPage = 0;
	private int printNowPage = 0;
	
	public int getPrintTotalPage() {
		return printTotalPage;
	}

	public void setPrintTotalPage(int printTotalPage) {
		this.printTotalPage = printTotalPage;
	}

	public int getPrintNowPage() {
		return printNowPage;
	}

	public void setPrintNowPage(int printNowPage) {
		this.printNowPage = printNowPage;
	}

	public boolean isMainWinRunning() {
		return mainWinRunning;
	}

	public void setMainWinRunning(boolean mainWinRunning) {
		this.mainWinRunning = mainWinRunning;
	}
	public boolean isPrinterPingable() {
		return printerPingable;
	}
	public void setPrinterPingable(boolean printerPingable) {
		this.printerPingable = printerPingable;
	}
	public boolean isPlatformHealth() {
		return platformHealth;
	}
	public void setPlatformHealth(boolean platformHealth) {
		this.platformHealth = platformHealth;
	}
	public InkInfo getInk() {
		return ink;
	}
	public void setInk(InkInfo ink) {
		this.ink = ink;
	}
	public float getWiper() {
		return wiper;
	}
	public void setWiper(float wiper) {
		this.wiper = wiper;
	}
	public ESystemStatus getStatus() {
		return status;
	}
	public void setStatus(ESystemStatus status) {
		this.status = status;
	}
	public EPrinterConnected getPrinterConnected() {
		return printerConnected;
	}
	public void setPrinterConnected(EPrinterConnected printerConnected) {
		this.printerConnected = printerConnected;
	}
	public boolean isTftpdRunning() {
		return tftpdRunning;
	}
	public void setTftpdRunning(boolean tftpdRunning) {
		this.tftpdRunning = tftpdRunning;
	}

	public List<Bzpa1Exception> getExceptions() {
		return exceptions;
	}

	public void setExceptions(List<Bzpa1Exception> exceptions) {
		this.exceptions = exceptions;
	}

}
