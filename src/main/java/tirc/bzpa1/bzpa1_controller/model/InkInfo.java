package tirc.bzpa1.bzpa1_controller.model;

public class InkInfo {
	private float c;
	private float m;
	private float y;
	private float k;
	
	public InkInfo() {
		
	}
	
	public InkInfo(float c, float m, float y, float k) {
		this.c = c;
		this.m = m;
		this.y = y;
		this.k = k;
	}
	
	public float getC() {
		return c;
	}
	public void setC(float c) {
		this.c = c;
	}
	public float getM() {
		return m;
	}
	public void setM(float m) {
		this.m = m;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		this.y = y;
	}
	public float getK() {
		return k;
	}
	public void setK(float k) {
		this.k = k;
	}
}
