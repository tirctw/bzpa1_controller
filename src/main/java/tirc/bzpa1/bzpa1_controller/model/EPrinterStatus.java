package tirc.bzpa1.bzpa1_controller.model;

public enum EPrinterStatus {
	OFF(0), 
	FAULT(1),
	TESTING(2),
	INITIALISING(3),
	DEPRIMED_IDLE(4),
	PRIMED_IDLE(5),
	SERVICING(6),
	PRE_JOB(7),
	PRINT_READY(8),
	PRINTING(9),
	MID_JOB(10),
	PAUSED(11),
	SESSION_COMPLETE(12),
	POST_JOB(13),
	SHUTTING_DOWN(14),
	PAUSING(15),
	NONE(16)
	;
	
	private final int sn ;
	
	private EPrinterStatus( int sn ) {
		this.sn = sn ;
	}
	
	public int getSN() {
		return sn ;
	}
	
	public static EPrinterStatus parse( int sn ) {
		for( EPrinterStatus o : values() ) {
			if ( o.getSN() == sn )
				return o ;
		}
		throw new RuntimeException("parse error!");
	}
}
