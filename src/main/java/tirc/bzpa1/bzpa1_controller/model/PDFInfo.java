package tirc.bzpa1.bzpa1_controller.model;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import tirc.bzpa1.bzpa1_controller.repository.BoxVariable;
import tirc.bzpa1.bzpa1_controller.repository.ProfileSize;

@ApiModel(description = "PDF資訊")
public class PDFInfo {
	@ApiModelProperty(value = "大小規格", required = false)
	private ProfileSize size;
	@ApiModelProperty(value = "變數設定", required = true)
	private List<BoxVariable> variables;
	
	public ProfileSize getSize() {
		return size;
	}
	public void setSize(ProfileSize size) {
		this.size = size;
	}
	public List<BoxVariable> getVariables() {
		return variables;
	}
	public void setVariables(List<BoxVariable> variables) {
		this.variables = variables;
	}

	
}
