package tirc.bzpa1.bzpa1_controller.model;

import java.util.List;

import tirc.bzpa1.bzpa1_controller.repository.BoxVariable;
import tirc.bzpa1.bzpa1_controller.repository.ProfileVariable;

public class PrintConfiguration{
	private String filePath;
	private PrintSetting settings;
	private List<ProfileVariable> variables;
	private String profileName;
	private boolean grayScale;
	
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public PrintSetting getSettings() {
		return settings;
	}
	public void setSettings(PrintSetting settings) {
		this.settings = settings;
	}
	public List<ProfileVariable> getVariables() {
		return variables;
	}
	public void setVariables(List<ProfileVariable> variables) {
		this.variables = variables;
	}
	public String getProfileName() {
		return profileName;
	}
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}
	public boolean isGrayScale() {
		return grayScale;
	}
	public void setGrayScale(boolean grayScale) {
		this.grayScale = grayScale;
	}
}
