package tirc.bzpa1.bzpa1_controller.model;

public class PlatformStatus {
    private boolean input;
    private boolean output;
    private boolean conveyor;
    private boolean error;
    private boolean emergency;
    private boolean shortage;
    private boolean full;
    private boolean stuck;
    private boolean discharge;
    private boolean grabLoop;
    private boolean printProcedureLoop;
    
    public PlatformStatus(boolean input, boolean output, boolean conveyor, boolean error, boolean emergency,
    		boolean shortage, boolean full, boolean stuck, boolean discharge, boolean grabLoop, boolean printProcedureLoop) {
    	this.input = input;
    	this.output = output;
    	this.conveyor = conveyor;
    	this.error = error;
    	this.emergency = emergency;
    	this.shortage = shortage;
    	this.full = full;
    	this.stuck = stuck;
    	this.discharge = discharge;
    	this.grabLoop = grabLoop;
    	this.printProcedureLoop = printProcedureLoop;
    }

	public boolean isGrabLoop() {
		return grabLoop;
	}

	public void setGrabLoop(boolean grabLoop) {
		this.grabLoop = grabLoop;
	}

	public boolean isPrintProcedureLoop() {
		return printProcedureLoop;
	}

	public void setPrintProcedureLoop(boolean printProcedureLoop) {
		this.printProcedureLoop = printProcedureLoop;
	}

	public boolean isShortage() {
		return shortage;
	}

	public void setShortage(boolean shortage) {
		this.shortage = shortage;
	}

	public boolean isFull() {
		return full;
	}

	public void setFull(boolean full) {
		this.full = full;
	}

	public boolean isStuck() {
		return stuck;
	}

	public void setStuck(boolean stuck) {
		this.stuck = stuck;
	}

	public boolean isDischarge() {
		return discharge;
	}

	public void setDischarge(boolean discharge) {
		this.discharge = discharge;
	}

	public boolean isInput() {
		return input;
	}

	public void setInput(boolean input) {
		this.input = input;
	}

	public boolean isOutput() {
		return output;
	}

	public void setOutput(boolean output) {
		this.output = output;
	}

	public boolean isConveyor() {
		return conveyor;
	}

	public void setConveyor(boolean conveyor) {
		this.conveyor = conveyor;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public boolean isEmergency() {
		return emergency;
	}

	public void setEmergency(boolean emergency) {
		this.emergency = emergency;
	}
}
