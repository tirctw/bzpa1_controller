package tirc.bzpa1.bzpa1_controller.model;

public enum EConveyorStatus {
	RUN(8), 
	STOP(0);
	
	private final int sn ;
	
	private EConveyorStatus( int sn ) {
		this.sn = sn ;
	}
	
	public int getSN() {
		return sn ;
	}
	
	public static EConveyorStatus parse( int sn ) {
		for( EConveyorStatus o : values() ) {
			if ( o.getSN() == sn )
				return o ;
		}
		throw new RuntimeException("parse error!");
	}
}
