package tirc.bzpa1.bzpa1_controller.model;

public enum EPrinterConnected {
	Online(0), 
	Offline(1);
	
	private final int sn ;
	
	private EPrinterConnected( int sn ) {
		this.sn = sn ;
	}
	
	public int getSN() {
		return sn ;
	}
	
	public static EPrinterConnected parse( int sn ) {
		for( EPrinterConnected o : values() ) {
			if ( o.getSN() == sn )
				return o ;
		}
		throw new RuntimeException("parse error!");
	}
}
