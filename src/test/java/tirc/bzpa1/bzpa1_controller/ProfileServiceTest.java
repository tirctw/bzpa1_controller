package tirc.bzpa1.bzpa1_controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.google.gson.Gson;

import tirc.bzpa1.bzpa1_controller.repository.Profile;
import tirc.bzpa1.bzpa1_controller.service.ProfileService;

@SpringBootTest
public class ProfileServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(ProfileServiceTest.class);
	private Gson gson = new Gson();
	
	@Autowired ProfileService profileService;

	@Test
	void tc001() throws IOException {		
	}
	
	@Test
	void tc002() throws FileNotFoundException {
		Profile p = profileService.deleteById((long)24);
		logger.info(gson.toJson(p));
	}
	
}

