package tirc.bzpa1.bzpa1_controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.google.gson.Gson;

import tirc.bzpa1.bzpa1_controller.model.EFace;
import tirc.bzpa1.bzpa1_controller.model.EIncrease;
import tirc.bzpa1.bzpa1_controller.model.PDFInfo;
import tirc.bzpa1.bzpa1_controller.model.PrintConfiguration;
import tirc.bzpa1.bzpa1_controller.model.PrintSetting;
import tirc.bzpa1.bzpa1_controller.repository.BoxVariable;
import tirc.bzpa1.bzpa1_controller.repository.ProfileSize;
import tirc.bzpa1.bzpa1_controller.repository.ProfileVariable;
import tirc.bzpa1.bzpa1_controller.service.BoxVariableService;
import tirc.bzpa1.bzpa1_controller.service.CreatePDFService;

@SpringBootTest
public class PDFServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(PDFServiceTest.class);
	private Gson gson = new Gson();
	
	@Autowired BoxVariableService boxVariableService;
	@Autowired CreatePDFService pdfService;

	@Test
	void tc001() {
		
		boxVariableService.update(new BoxVariable("${PO_NO_1}", "PO NO 1"));
		boxVariableService.update(new BoxVariable("${PART_NO_1}", "PART NO 1"));
		boxVariableService.update(new BoxVariable("${QTY_1}", "QTY 1"));
		boxVariableService.update(new BoxVariable("${CARTON_NUM_1}", "CARTON NUM 1"));
		logger.info("=== Find all box variable ===");
		logger.info(gson.toJson(boxVariableService.findAll()));
		
		try {
			PDFInfo info = pdfService.findInfo("InkScape2.pdf");
			logger.info(gson.toJson(info));			
			
		} catch (IOException e) {
			logger.info("File not found.");
			e.printStackTrace();
		}
	}
	

	@Test
	void tc002() throws Exception {
		

		try {
			
			PrintConfiguration config = new PrintConfiguration();
			PrintSetting setting = new PrintSetting();
			
			config.setFilePath("xchange_test1.1.pdf");
			
			List<ProfileVariable> list = new ArrayList<>();
			
			list.add(new ProfileVariable("${ITEM}", "item", "Taiwan", EIncrease.NONE, 0L, EFace.ALL, ""));
			list.add(new ProfileVariable("${CTN}", "CartonNum", "123", EIncrease.NONE, 0L, EFace.ALL, ""));
			
			config.setVariables(list);
			
		
			setting.setCopies(1);
			setting.setSize(new ProfileSize(100, 300, EFace.ALL));
			
			config.setSettings(setting);
			
			pdfService.generatePrintPDF(config, null);
			
		} catch (IOException e) {
			logger.info("File not found.");
			e.printStackTrace();
		}
	}

	
}
