package tirc.bzpa1.bzpa1_controller;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.google.gson.Gson;

import tirc.bzpa1.bzpa1_controller.repository.BoxVariable;
import tirc.bzpa1.bzpa1_controller.service.BoxVariableService;

@SpringBootTest
public class BoxVariableServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(BoxVariableServiceTest.class);
	private Gson gson = new Gson();
	
	@Autowired BoxVariableService boxVariableService;
	
	@Test
	void tc001() {
		logger.info("size1:{}", boxVariableService.findAll().size());
		boxVariableService.update(new BoxVariable("${PART_NO_1}", "P0 1223"));
		boxVariableService.update(new BoxVariable("${PO_NO_3}", "PO 5678"));
		
		boxVariableService.update(new BoxVariable("45889", "測試繁體中文"));
		
		logger.info("size2:{}", boxVariableService.findAll().size());
		for(BoxVariable val:boxVariableService.findAll()) {
			logger.info(gson.toJson(val));
		}
		
	}
	
	@Test
	void tc002() throws FileNotFoundException {
		logger.info("BoxVariable:{}", gson.toJson(boxVariableService.deleteByKey("b7")));
	}
}
