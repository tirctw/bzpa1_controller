package tirc.bzpa1.bzpa1_controller;

import java.text.ParseException;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.google.gson.Gson;

import tirc.bzpa1.bzpa1_controller.model.ECategory;
import tirc.bzpa1.bzpa1_controller.model.ELevel;
import tirc.bzpa1.bzpa1_controller.repository.Log;
import tirc.bzpa1.bzpa1_controller.service.LogService;

@SpringBootTest
public class LogServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(LogServiceTest.class);
	private Gson gson = new Gson();
	
	@Autowired LogService logService;
	
	@Test
	void tc001() {
		logService.create(ECategory.PLATFORM, ELevel.DEBUG, "2100 msg");
		logService.create(ECategory.PRINTER, ELevel.ERROR, "2300 msg");
	}
	
	@Test
	void tc002() throws ParseException {
		logService.findByFilter("INFO", "PRINTER", "2021-06-04", "2021-06-05");
	}
}
