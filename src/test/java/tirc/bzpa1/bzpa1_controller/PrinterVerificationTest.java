package tirc.bzpa1.bzpa1_controller;

import java.io.File;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import com.google.gson.Gson;

import tirc.bzpa1.bzpa1_controller.model.Bzpa1Exception;
import tirc.bzpa1.bzpa1_controller.model.Bzpa1Exception.Reason;
import tirc.bzpa1.bzpa1_controller.model.Settings;
import tirc.bzpa1.bzpa1_controller.service.SettingService;
import tirc.bzpa1.bzpa1_controller.service.SocketService;

@SpringBootTest
public class PrinterVerificationTest {
	private static final Logger logger = LoggerFactory.getLogger(PrinterVerificationTest.class);
	private Gson gson = new Gson();
	private Settings settings = SettingService.getInstance().read();
	private static int count = 1;
	
	@Autowired SocketService socketService;
	
	@Test
	@RepeatedTest(100)
	void tc001() {
		logger.info("========= 執行第" + count++ + "次測試 ===========");
		
		logger.info("檢查噴印機初始是否為 'PRIMED_IDLE'或 'SERVICING' 狀態");
		Assert.isTrue(getResponse("Getstatus").contains("PRIMED_IDLE") ||
				getResponse("Getstatus").contains("SERVICING"), "");
		
		logger.info("檢查SetToOffline cmd是否正確回傳");
		Assert.isTrue(getResponse("SetToOffline").contains("Success"), "");
		
		logger.info("檢查狀態是否切換為offline");
		Assert.isTrue(getResponse("Getstatus").contains("Offline"), "");
		
		logger.info("檢查SetToOnline cmd是否正確回傳");
		Assert.isTrue(getResponse("SetToOnline").contains("Success"), "");
		
		logger.info("檢查狀態是否切換為online");
		Assert.isTrue(getResponse("Getstatus").contains("Online"), "");
		
		logger.info("檢查Cink格式是否正確");
		Assert.isTrue(getResponse("GetCink").contains("%"), "");
		
		logger.info("檢查Cink格式是否正確");
		Assert.isTrue(getResponse("GetCink").contains("%"), "");

		logger.info("檢查Mink格式是否正確");
		Assert.isTrue(getResponse("GetMink").contains("%"), "");
		
		logger.info("檢查Yink格式是否正確");
		Assert.isTrue(getResponse("GetYink").contains("%"), "");
		
		logger.info("檢查Kink格式是否正確");
		Assert.isTrue(getResponse("GetKink").contains("%"), "");
		
		logger.info("檢查Wiper格式是否正確");
//		Assert.isTrue(getResponse("GetWiper").contains("%"), "");
		
		logger.info("檢查列印檔案是否存在");
		String path = "C:\\bzpa1\\printPDF.pdf";
		Assert.isTrue(new File(path).exists(), "");
		
		logger.info("檢查Print cmd是否正確回傳");
		String printCmd =  "Printpdf," + path + ",1,1";
		Assert.isTrue(getResponse(printCmd).contains("Success"), "");
		
		logger.info("等待 進入PRINTING status");
		int printTimeoutCount = 0;
		while(!getResponse("Getstatus").contains("PRINTING")) {
			if(printTimeoutCount++ > 30)
				throw new IllegalStateException("Timeout");
			sleep(1000);
		}
		logger.info("等待 timeout");
		sleep(20000);
		
		logger.info("檢查狀態是否為Error");
		Assert.isTrue(getResponse("Getstatus").contains("Error"), "");
		
		logger.info("檢查ClearFail是否正確回傳");
		Assert.isTrue(getResponse("ClearFail").contains("Success"), "");
		
		logger.info("等待 清除錯誤");
		sleep(10000);
		
		logger.info("檢查狀態是否已修復錯誤");
		Assert.isTrue(!getResponse("Getstatus").contains("Error"), "");
		
//		logger.info("檢查Print cmd是否正確回傳");
//		Assert.isTrue(getResponse(printCmd).contains("Success"), "");
//		
//		logger.info("等待 進入PRINTING status");
//		printTimeoutCount = 0;
//		while(!getResponse("Getstatus").contains("PRINTING")) {
//			if(printTimeoutCount++ > 30)
//				throw new IllegalStateException("Timeout");
//			sleep(1000);
//		}
//		
//		logger.info("檢查ResetQueue cmd是否正確回傳");
//		Assert.isTrue(getResponse("ResetQueue").contains("Success"), "");
//		
//		logger.info("檢查噴印機初始是否為 'PRIMED_IDLE'或 'SERVICING' 狀態");
//		Assert.isTrue(getResponse("Getstatus").contains("PRIMED_IDLE") ||
//				getResponse("Getstatus").contains("SERVICING"), "");
		
		sleep(10000);
	}
	
	private String getResponse(String command) {
		String response = socketService.sendCommand(command, settings.getPrinterPort());
		logger.info("Cmd:{} , Response:{}", command, response);
		
		if(response == null || response.contains("Fail")) {
			throw new Bzpa1Exception(Reason.PRINTER_RESPONSE_FAIL);
		}		
		return response;
	}
	
	private void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
